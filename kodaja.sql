-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2018 at 02:48 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kodaja`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank`
--

CREATE TABLE `tb_bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(55) NOT NULL,
  `atas_nama` varchar(60) NOT NULL,
  `no_rek` varchar(50) NOT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bank`
--

INSERT INTO `tb_bank` (`id_bank`, `nama_bank`, `atas_nama`, `no_rek`, `image`) VALUES
(29, 'BNI', 'PT. Coba Indonesia', '355464335735325255', 'http://kodaja.levanpedia.com/assets/upload/bank/Logo_Bank_BNI_JPG.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_contact`
--

CREATE TABLE `tb_contact` (
  `id_contact` int(1) NOT NULL,
  `alamat` text NOT NULL,
  `contact_person` text NOT NULL,
  `legal_information` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_contact`
--

INSERT INTO `tb_contact` (`id_contact`, `alamat`, `contact_person`, `legal_information`) VALUES
(1, '<p>Bld Mihail Kogalniceanu, nr. 8,&nbsp;<br />\r\n7652 Bucharest,&nbsp;<br />\r\nRomania</p>\r\n', '<p>Michael Jordan&nbsp;<br />\r\n+40 762 321 762&nbsp;<br />\r\nMon - Fri, 8:00-22:00</p>\r\n', '<p>Creative Tim Ltd.&nbsp;<br />\r\nVAT &middot; EN2341241&nbsp;<br />\r\nIBAN &middot; EN8732ENGB2300099123&nbsp;<br />\r\nBank &middot; Great Britain Bank</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cost`
--

CREATE TABLE `tb_cost` (
  `id_cost` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cost`
--

INSERT INTO `tb_cost` (`id_cost`, `price`, `kategori`) VALUES
(3, '12000', '0-3 km'),
(4, '10000', '>3 km'),
(5, '20000', 'weekend');

-- --------------------------------------------------------



--
-- Table structure for table `tb_dana`
--

CREATE TABLE `tb_dana` (
  `id_dana` int(11) NOT NULL,
  `invoice_id` varchar(20) NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `kodaja` decimal(10,0) NOT NULL,
  `adat` decimal(10,0) NOT NULL,
  `driver` decimal(10,0) NOT NULL,
  `status_customer` set('accept','cancel','pending') NOT NULL DEFAULT 'pending',
  `status_driver` set('accept','cancel','pending') NOT NULL DEFAULT 'pending',
  `create_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dana`
--

INSERT INTO `tb_dana` (`id_dana`, `invoice_id`, `cost`, `kodaja`, `adat`, `driver`, `status_customer`, `status_driver`, `create_on`) VALUES
(9, '115997', '50000', '2500', '5000', '42500', 'pending', 'pending', '2018-03-19 23:23:36'),
(10, '277954', '50000', '5000', '5000', '40000', 'pending', 'pending', '2018-03-19 23:25:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_history`
--

CREATE TABLE `tb_history` (
  `id_history` int(11) NOT NULL,
  `invoice_id` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_history`
--

INSERT INTO `tb_history` (`id_history`, `invoice_id`, `status`, `user_id`, `create_on`) VALUES
(1, '687317', 'Cancel', 29, '2018-03-18 00:30:31'),
(2, '125244', 'Success', 29, '2018-03-18 00:42:04'),
(3, '688477', 'Success', 35, '2018-03-18 02:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_invoice`
--

CREATE TABLE `tb_invoice` (
  `kode` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `destination` varchar(50) NOT NULL,
  `origin` text NOT NULL,
  `cost` int(11) NOT NULL,
  `wisata_id` int(11) DEFAULT NULL,
  `wisata` text,
  `status` varchar(20) NOT NULL,
  `category` set('wisata','driver') DEFAULT NULL,
  `create_on` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_invoice`
--

INSERT INTO `tb_invoice` (`kode`, `user_id`, `driver_id`, `destination`, `origin`, `cost`, `wisata_id`, `wisata`, `status`, `category`, `create_on`) VALUES
('115997', 35, 34, 'ghntdrhbern', 'aewtew', 50000, 0, '', 'unpaid', 'driver', '2018-03-19 23:23:36'),
('277954', 29, 34, 'ghntdrhbern', 'aewtew', 50000, 4, '[''adult''=>2,''child''=>1,''note''=>''kakjaha'',]', 'unpaid', 'wisata', '2018-03-19 23:25:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kendaraan`
--

CREATE TABLE `tb_kendaraan` (
  `id_kendaraan` int(10) NOT NULL,
  `merk` varchar(20) NOT NULL,
  `model` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `tahun` int(10) NOT NULL,
  `plat` varchar(20) NOT NULL,
  `wilayah` varchar(50) NOT NULL,
  `foto_kendaraan` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kendaraan`
--

INSERT INTO `tb_kendaraan` (`id_kendaraan`, `merk`, `model`, `warna`, `tahun`, `plat`, `wilayah`, `foto_kendaraan`, `user_id`) VALUES
(1, 'Toyota', 'Avanza', 'Putih', 2015, 'DK 1587 SK', 'Ubud', 'index2.jpg index3.jpg index4.jpg index5.jpg ', 34);

-- --------------------------------------------------------

--
-- Table structure for table `tb_news`
--

CREATE TABLE `tb_news` (
  `id_news` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_news`
--

INSERT INTO `tb_news` (`id_news`, `judul`, `isi`, `image`, `tanggal`) VALUES
(20, 'Banyuwangi Indah', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'http://kodaja.levanpedia.com/assets/upload/news/Iguana.jpg', '2018-03-06'),
(21, 'VYAN', 'kksnvssnvkllskl dinbdinbn bdbdvdn egengie eeoengeogopeg egeojge eeogjeopg', 'http://kodaja.levanpedia.com/assets/upload/news/Iguana.jpg', '2018-03-12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pages`
--

CREATE TABLE `tb_pages` (
  `id_pages` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `sub_judul` text NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pages`
--

INSERT INTO `tb_pages` (`id_pages`, `judul`, `sub_judul`, `kategori`, `isi`, `image`, `tanggal`) VALUES
(1, 'Wellcome To Our Website', '-', 'website', '<p style="text-align:justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>\r\n', 'http://127.0.0.1/proyek/wisata/assets/upload/pages/Kodaja_Logo-2.png', '2018-03-15'),
(2, 'About Us', 'Meet the amazing team behind this project and find out more about how we work.', 'website', '<p style="text-align:justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\r\n<br />\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n', 'http://127.0.0.1/proyek/wisata/assets/upload/pages/006434800_1444821384-pura_besakih.jpg', '2018-03-15'),
(3, 'Paket Wisata', 'Berbagai destinasi wisata yang telah kami siapkan untuk Anda dan keluarga. Are you ready for adventure?', 'website', '<p>-</p>\r\n', 'http://127.0.0.1/proyek/wisata/assets/upload/pages/profile_city.jpg', '2018-03-18'),
(4, 'Contact Us', 'Meet the amazing team behind this project and find out more about how we work.', 'website', '<p>-</p>\r\n', 'http://127.0.0.1/proyek/wisata/assets/upload/pages/bg.jpg', '2018-03-18'),
(5, 'Coba Android', '-', 'android', '<p>rgrgr r gegregrre r r eh reh r e r rrg rghreh&nbsp;</p>\r\n', 'http://127.0.0.1/proyek/wisata/assets/upload/pages/city.jpg', '2018-03-18');

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment`
--

CREATE TABLE `tb_payment` (
  `id_payment` int(11) NOT NULL,
  `kode` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `atas_nama` varchar(50) NOT NULL,
  `bank` varchar(40) NOT NULL,
  `no_rek` varchar(20) NOT NULL,
  `date` date DEFAULT NULL,
  `images` text NOT NULL,
  `category` varchar(20) NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_payout`
--

CREATE TABLE `tb_payout` (
  `id_payout` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nominal` decimal(10,0) NOT NULL,
  `status` set('approve','unapprove') NOT NULL DEFAULT 'unapprove',
  `create_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_slideshow`
--

CREATE TABLE `tb_slideshow` (
  `id_slide` int(11) NOT NULL,
  `nama_slide` varchar(50) NOT NULL,
  `image` varchar(200) NOT NULL,
  `text` text,
  `kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_slideshow`
--

INSERT INTO `tb_slideshow` (`id_slide`, `nama_slide`, `image`, `text`, `kategori`) VALUES
(7, 'Lovina Beach', 'http://kodaja.levanpedia.com/assets/upload/slideshow/lovina-beach-bali.png', '<p><strong>Lovina Beach</strong> (or often simply Lovina) is a coastal area on the northwestern side of the island of Bali, Indonesia.</p>\r\n', 'website'),
(9, 'Bedugul', 'http://kodaja.levanpedia.com/assets/upload/slideshow/bedugul-bali-panorama-960x472.jpg', '<p><strong>Bedugul </strong>is a mountain lake resort area in Bali,[1] Indonesia, located in the centre-north region of the island near Lake Bratan on the road between Denpasar and Singaraja the area covers the villages of Bedugul itself, Candikuning, Pancasari, Pacung and Wanagiri amongst others.</p>\r\n', 'website'),
(11, 'Kintamani', 'http://kodaja.levanpedia.com/assets/upload/slideshow/kintamani1.jpg', '<p><strong>Kintamani </strong>is a village on the western edge of the larger caldera wall of Gunung Batur in Bali, Indonesia.</p>\r\n', 'website'),
(12, 'Kintamani', 'http://kodaja.levanpedia.com/assets/upload/slideshow/kintamani.jpg', '<p><strong>Kintamani </strong>is a village on the western edge of the larger caldera wall of Gunung Batur in Bali, Indonesia.</p>\r\n', 'android'),
(13, 'Bedugul', 'http://kodaja.levanpedia.com/assets/upload/slideshow/bedugul-bali-panorama-960x4721.jpg', '<p><strong>Bedugul&nbsp;</strong>is a mountain lake resort area in Bali,[1] Indonesia, located in the centre-north region of the island near Lake Bratan on the road between Denpasar and Singaraja the area covers the villages of Bedugul itself, Candikuning, Pancasari, Pacung and Wanagiri amongst others.</p>\r\n', 'android'),
(14, 'Lovina', 'http://kodaja.levanpedia.com/assets/upload/slideshow/lovina-beach-bali1.png', '<p><strong>Lovina Beach</strong> (or often simply Lovina) is a coastal area on the northwestern side of the island of Bali, Indonesia.</p>\r\n', 'android');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sosmed`
--

CREATE TABLE `tb_sosmed` (
  `id_sosmed` int(11) NOT NULL,
  `nama_sosmed` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_topup`
--

CREATE TABLE `tb_topup` (
  `kode` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nominal` decimal(10,0) NOT NULL,
  `pembayaran` varchar(50) NOT NULL,
  `status_topup` set('approve','unapprove') NOT NULL DEFAULT 'unapprove'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_topup`
--

INSERT INTO `tb_topup` (`kode`, `id_user`, `nominal`, `pembayaran`, `status_topup`) VALUES
(0, 29, '10000', 'transfer', 'unapprove'),
(1, 29, '1000000', 'ATM', 'approve'),
(2, 29, '500000', 'ATM', 'approve');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_lahir` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `sim` varchar(20) NOT NULL,
  `kategori` varchar(15) NOT NULL,
  `foto_user` varchar(200) NOT NULL,
  `lat` text NOT NULL,
  `lang` text NOT NULL,
  `ket` text NOT NULL,
  `date` date NOT NULL,
  `token` varchar(200) NOT NULL,
  `firebase` text,
  `saldo` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama`, `alamat`, `tgl_lahir`, `email`, `password`, `no_hp`, `status`, `sim`, `kategori`, `foto_user`, `lat`, `lang`, `ket`, `date`, `token`, `firebase`, `saldo`) VALUES
(20, 'COBA ATUH ', 'scbskbvkbs', '03/06/2018', 'superadmin@mail.com', 'vEq2fOGk4KbkmJutoTnGDTZRjlxwminDYi6RorMNhgS0ow2SBPTS41diH7Dlv1CPosDlwDEH4Gvk53sMaG5JqQ==', '35325325', 'active', 'SIM A', 'driver', 'http://kodaja.levanpedia.com/assets/upload/user/admin.png', '', '', 'dbdbddbdsb', '2018-03-09', '', NULL, '0'),
(21, 'admin', 'snsvns', '03/28/2018', 'admin@kodaja.com', 'awGxgALpdOP4ZAOdqLE5PQ9N1JgAfn/XZhPeCQcgWsykNxPUs9gqqi8vtEyvD8UwnXsMUvTD6yEzRt2MsHbMHw==', '799', 'active', '', 'admin', 'http://kodaja.levanpedia.com/assets/upload/user/Picture1.png', '', '', '', '2018-03-10', '63jjkb7b5', NULL, '0'),
(22, 'admin KODAJA 1', 'scscsccsc', '03/27/2018', 'admin1@kodaja.com', 'Zj8rzTl//4yJy0Bfs+jarnQp93mkZEwnsBW3bkNkXJmPoLxleV7WW80FIJYWW1BmELv6DZEENl1Axz1tjlMXFA==', '45543535', 'active', '', 'admin', 'http://kodaja.levanpedia.com/assets/upload/user/Picture4.png', '', '', '', '2018-03-10', '63jjkb7b5', NULL, '0'),
(29, 'vyan', 'scsscscs', 'cscscs', 'vyan@gmail.com', 'XRLaDsQqaJ7ddzBiVJ9DwVUuOAGA/RpzPyXG4nGtpgQ7vizgOP6EDTcKC8AKWccDyluRtQoitwRQ2CNmdGL63g==', '34234434', 'active', '', 'customer', '', '', '', '', '2018-03-12', 'svfsaugfbeibgb', NULL, '2500000'),
(34, 'I Wayan Agus Merthayasa', 'Iseh, Sidemen, Karangasem', '03/20/2018', 'merthayasaagus86@gmail.com', '4Zl/9XQOALqCCrTfoYdjd/wbZu923ZJHhJw2UVn6hRAtyLJeeOUoxumEdk3KrhgZhiFGsFupz05mNzC5rdeF0A==', '+6281337648755', 'active', 'SIM A UMUM', 'driver', 'http://127.0.0.1/proyek/wisata/assets/upload/user/index.jpg', '', '', '-', '2018-03-15', 'svfsaugfbeibgb', NULL, '53458500'),
(35, 'Ary', 'scsscscs', 'cscscs', 'vyan1111@gmail.com', 'RRFjku/EYeLZO1pVeSOEM+tDVihke/YeBOEHU7vGPOt0opnI3K1Gv+xRWNTBbWk3wsSbhDorroTCM7aEUkusMg==', '34234434', 'active', '', 'customer', '', '', '', '', '2018-03-18', 'svfsaugfbeibgb', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_wisata`
--

CREATE TABLE `tb_wisata` (
  `id_wisata` int(11) NOT NULL,
  `nama_wisata` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `fasilitas` text NOT NULL,
  `harga` int(50) NOT NULL,
  `image` varchar(200) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_wisata`
--

INSERT INTO `tb_wisata` (`id_wisata`, `nama_wisata`, `keterangan`, `fasilitas`, `harga`, `image`, `date`) VALUES
(4, 'KINTAMANI - VOLCANO TOUR', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 09.00 am</p>\r\n\r\n<p>Approximate 6&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Goa Gajah</td>\r\n			<td>:</td>\r\n			<td>Elephant Cave Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Gunung Kawi</td>\r\n			<td>:</td>\r\n			<td>Rocky Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tampaksiring</td>\r\n			<td>:</td>\r\n			<td>The Holly Spring Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Temen</td>\r\n			<td>:</td>\r\n			<td>Speces and organic coffe plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Kintamani</td>\r\n			<td>:</td>\r\n			<td>Volcano and lake crater view</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tegallalang</td>\r\n			<td>:</td>\r\n			<td>Rice Terrace field view</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>svsvjsvjskvjsv</p>\r\n', 550000, 'http://kodaja.levanpedia.com/assets/upload/wisata/kintamani.jpg', '2018-03-13'),
(5, 'KINTAMANI - BESAKIH TOUR', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 09.00 am</p>\r\n\r\n<p>Approximate 8&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Goa Gajah</td>\r\n			<td>:</td>\r\n			<td>Elephant Cave Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tampaksiring</td>\r\n			<td>:</td>\r\n			<td>The Holly Spring Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Temen</td>\r\n			<td>:</td>\r\n			<td>Speces and organic coffe plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Kintamani</td>\r\n			<td>:</td>\r\n			<td>Volcano and lake crater view</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Besakih</td>\r\n			<td>:</td>\r\n			<td>The Largest Hindu temple in Bali</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Klungkung</td>\r\n			<td>:</td>\r\n			<td>Kertha Gosa, Royal Court Justice</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>vdvdvdvdvdvdvdvvdsvhj</p>\r\n', 590000, 'http://kodaja.levanpedia.com/assets/upload/wisata/A_view_of_entrance_stairs_to_Pura_Besakih_Hindu_Temple_Bali_Indonesia.jpg', '2018-03-13'),
(6, 'BEDUGUL - JATILUWIH TOUR', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 09.00 am</p>\r\n\r\n<p>Approximate 7&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Perean</td>\r\n			<td>:</td>\r\n			<td>Spices and organic coffe plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Bedugung</td>\r\n			<td>:</td>\r\n			<td>Beautiful Temple on lake</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Jatiluwih</td>\r\n			<td>:</td>\r\n			<td>The best Rice Terrace in Bali</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Mengwi</td>\r\n			<td>:</td>\r\n			<td>Royal Famili Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Lodtunduh</td>\r\n			<td>:</td>\r\n			<td>Painting Village</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>vdvvdv</p>\r\n', 590000, 'http://kodaja.levanpedia.com/assets/upload/wisata/celuk7.gif', '2018-03-13'),
(7, 'TANAH LOT SUNSET TOUR', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 14.00&nbsp;</p>\r\n\r\n<p>Approximate 6&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Lodtunduh</td>\r\n			<td>:</td>\r\n			<td>Painting Village</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Singakerta</td>\r\n			<td>:</td>\r\n			<td>Coffe Traditional Processing and Plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Mengwi</td>\r\n			<td>:</td>\r\n			<td>Royal Family temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Alas Kedaton</td>\r\n			<td>:</td>\r\n			<td>Huge Bat and Mongkey Forest</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tanah Lot</td>\r\n			<td>:</td>\r\n			<td>Temple on sea &amp; wonderfull sunset</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>vdvdv</p>\r\n', 590000, 'http://kodaja.levanpedia.com/assets/upload/wisata/Tanah-Lot-Sunset-Tour-Bali-Half-Day-Tour.jpg', '2018-03-13'),
(8, 'BEKASIH TOUR', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 09.00 am</p>\r\n\r\n<p>Approximate 7&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Goa Gajah</td>\r\n			<td>:</td>\r\n			<td>Elephant Cave Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Bangli</td>\r\n			<td>:</td>\r\n			<td>Kehen Temple on the sloope of the hill</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Bukit Jambul</td>\r\n			<td>:</td>\r\n			<td>Wonderfull view</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Besakih</td>\r\n			<td>:</td>\r\n			<td>The Largest Hindu temple in Bali</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Klungkung</td>\r\n			<td>:</td>\r\n			<td>Kertha Gosa, Royal Court Justice</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>vdvdvdv</p>\r\n', 550000, 'http://kodaja.levanpedia.com/assets/upload/wisata/006434800_1444821384-pura_besakih.jpg', '2018-03-13'),
(9, 'KINTAMANI WATERFALL & BEDUGUL TOUR', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 08.30 am</p>\r\n\r\n<p>Approximate 9&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Tampaksiring</td>\r\n			<td>:</td>\r\n			<td>The Holly Spring Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Temen</td>\r\n			<td>:</td>\r\n			<td>Speces and organic coffe plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Kintamani</td>\r\n			<td>:</td>\r\n			<td>Volcano and lake crater view</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Nung nung</td>\r\n			<td>:</td>\r\n			<td>The highest Waterfall in island</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Plaga Village</td>\r\n			<td>:</td>\r\n			<td>The Highest Bridge</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Bedugul</td>\r\n			<td>:</td>\r\n			<td>Beatifull temple on the lake</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>bdbdbd</p>\r\n', 790000, 'http://kodaja.levanpedia.com/assets/upload/wisata/sekumpul-waterfall-1.jpg', '2018-03-13'),
(10, 'UBUD SURROUNDING TOUR', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 08.30 am</p>\r\n\r\n<p>Approximate 6&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Batubulan</td>\r\n			<td>:</td>\r\n			<td>Traditional Balinese Barong Dance</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Celuk</td>\r\n			<td>:</td>\r\n			<td>Gold and Silver Smith and shop</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tegenungan</td>\r\n			<td>:</td>\r\n			<td>Magnificent Waterfall</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Goa Gajah</td>\r\n			<td>:</td>\r\n			<td>Elephant Cave &amp; temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tampaksiring</td>\r\n			<td>:</td>\r\n			<td>The Holly Spring Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Temen</td>\r\n			<td>:</td>\r\n			<td>Speces and organic coffe plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tegallalang</td>\r\n			<td>:</td>\r\n			<td>Rice Terrace field</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>fbfbfb</p>\r\n', 550000, 'http://kodaja.levanpedia.com/assets/upload/wisata/Screenshot_3.jpg', '2018-03-13'),
(11, 'KINTAMANI BANGLI TOUR', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 09.00 am</p>\r\n\r\n<p>Approximate 8&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Goa Gajah</td>\r\n			<td>:</td>\r\n			<td>Elephant Cave &amp; temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Tampaksiring</td>\r\n			<td>:</td>\r\n			<td>The Holly Spring Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Temen</td>\r\n			<td>:</td>\r\n			<td>Speces and organic coffe plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Kintamani</td>\r\n			<td>:</td>\r\n			<td>Volcano and lake carter view</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Penglipuran</td>\r\n			<td>:</td>\r\n			<td>Traditional Village</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Bangli</td>\r\n			<td>:</td>\r\n			<td>Keben Temple</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>bddbdbdb</p>\r\n', 650000, 'http://kodaja.levanpedia.com/assets/upload/wisata/Desa-Penglipuran-Bangli-Bali-Feature.jpg', '2018-03-13'),
(12, 'SINGARAJA - LOVINA TOUR ', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque euismod, commodo urna eget, consectetur dolor. Phasellus ut nunc erat. Duis mattis a urna sed posuere. Integer maximus quis erat a efficitur. Sed ultrices commodo bibendum. Morbi id sapien turpis. Aliquam vitae arcu magna. Integer at mi lobortis enim placerat vehicula vitae vel mauris.</p>\r\n\r\n<p>Dept Time : 08.30 am</p>\r\n\r\n<p>Approximate 9&nbsp;Hrs</p>\r\n\r\n<table border="0" cellpadding="1" cellspacing="1">\r\n	<tbody>\r\n		<tr>\r\n			<td>- Mengwi</td>\r\n			<td>:</td>\r\n			<td>Royal Family Temple</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Perean</td>\r\n			<td>:</td>\r\n			<td>Spices and organic coffe plantation</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Bedugul</td>\r\n			<td>:</td>\r\n			<td>Beautiful temple on the lake</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Gitgit</td>\r\n			<td>:</td>\r\n			<td>Great Waterfall</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Lovina</td>\r\n			<td>:</td>\r\n			<td>Black sand norther beach</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Banjar</td>\r\n			<td>:</td>\r\n			<td>Natural Hot Springs</td>\r\n		</tr>\r\n		<tr>\r\n			<td>- Munduk</td>\r\n			<td>:</td>\r\n			<td>Nice View of nothern</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<p>vdvdvdvdvd</p>\r\n', 790000, 'http://kodaja.levanpedia.com/assets/upload/wisata/Singaraja-symbol.jpg', '2018-03-13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `tb_contact`
--
ALTER TABLE `tb_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `tb_cost`
--
ALTER TABLE `tb_cost`
  ADD PRIMARY KEY (`id_cost`);

--
-- Indexes for table `tb_dana`
--
ALTER TABLE `tb_dana`
  ADD PRIMARY KEY (`id_dana`);

--
-- Indexes for table `tb_history`
--
ALTER TABLE `tb_history`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD PRIMARY KEY (`kode`),
  ADD UNIQUE KEY `kode_invoice` (`kode`);

--
-- Indexes for table `tb_kendaraan`
--
ALTER TABLE `tb_kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `tb_news`
--
ALTER TABLE `tb_news`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `tb_pages`
--
ALTER TABLE `tb_pages`
  ADD PRIMARY KEY (`id_pages`);

--
-- Indexes for table `tb_payment`
--
ALTER TABLE `tb_payment`
  ADD PRIMARY KEY (`id_payment`);

--
-- Indexes for table `tb_payout`
--
ALTER TABLE `tb_payout`
  ADD PRIMARY KEY (`id_payout`);

--
-- Indexes for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indexes for table `tb_sosmed`
--
ALTER TABLE `tb_sosmed`
  ADD PRIMARY KEY (`id_sosmed`);

--
-- Indexes for table `tb_topup`
--
ALTER TABLE `tb_topup`
  ADD PRIMARY KEY (`kode`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_wisata`
--
ALTER TABLE `tb_wisata`
  ADD PRIMARY KEY (`id_wisata`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tb_contact`
--
ALTER TABLE `tb_contact`
  MODIFY `id_contact` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_cost`
--
ALTER TABLE `tb_cost`
  MODIFY `id_cost` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_dana`
--
ALTER TABLE `tb_dana`
  MODIFY `id_dana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_history`
--
ALTER TABLE `tb_history`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_kendaraan`
--
ALTER TABLE `tb_kendaraan`
  MODIFY `id_kendaraan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_news`
--
ALTER TABLE `tb_news`
  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_pages`
--
ALTER TABLE `tb_pages`
  MODIFY `id_pages` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_payment`
--
ALTER TABLE `tb_payment`
  MODIFY `id_payment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_payout`
--
ALTER TABLE `tb_payout`
  MODIFY `id_payout` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  MODIFY `id_slide` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_sosmed`
--
ALTER TABLE `tb_sosmed`
  MODIFY `id_sosmed` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_wisata`
--
ALTER TABLE `tb_wisata`
  MODIFY `id_wisata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
