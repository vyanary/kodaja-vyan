<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	
class Home extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_slideshow');
        $this->load->model('M_wisata');
        $this->load->model('M_pages');
        $this->load->model('M_contact');
    }

	public function index(){
        $home = $this->M_pages->get_by_kat_and_id('website',1);
		$slideshow = $this->M_slideshow->get_by_kat('website');
		$totalslideshow = $this->M_slideshow->total_rows('website');
		$wisata = $this->M_wisata->get_limit_data(6);

        $data = array(
            'data_slideshow' => $slideshow,
            'total_slideshow'=> $totalslideshow,
            'data_wisata' => $wisata,
            'judul' => $home->judul,
            'isi' => $home->isi,
            'image' => $home->image,
            'page_title' => " ", 
        );
		$this->load->view('frontend/home', $data);
	}
}
