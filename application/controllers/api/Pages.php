<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Pages extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_pages');
    }

    function index_get(){
        $id_pages=$this->input->get('id_pages');
        if ($id_pages!="") {
            $pages=$this->M_pages->get_by_kat_and_id_result('android',$id_pages);
            $this->response([
                'kode' => 1,
                'result' => $pages,
                'pesan' =>'Data tidak kosong!'
            ], REST_Controller::HTTP_OK); 
        }else{
            $pages=$this->M_pages->get_by_kat('android');
            $this->response([
                'kode' => 1,
                'result' => $pages,
                'pesan' =>'Data tidak kosong!'
            ], REST_Controller::HTTP_OK); 
        }
    }
    
}