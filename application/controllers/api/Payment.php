<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Payment extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_payment','M_user'));
    }

    function index_post(){
        $id = $this->post('id');
        $token = $this->post('token');
        $kode = $this->post('kode');
        $atas_nama = $this->post('nama');
        $bank = $this->post('bank');
        $no_rek = $this->post('rek');
        $date = $this->post('tanggal');
        $images = $this->post('images');
        $category = $this->post('category');
        
        
        
        $where = array('id_user' => $id, );
        $user_detail = $this->M_user->get_by_id($id);
                if ($this->M_user->cek_user($where)->num_rows() > 0) {
                    if($user_detail->token != $token){
                        $this->response(['kode' => 0,'pesan' =>'Anda tidak memiliki akses!'], REST_Controller::HTTP_OK);
                    }else{
                        $data = array(
                                'id_user' => $id,
                                'kode' =>$kode,
                                'atas_nama' => $atas_nama,
                                'bank' => $bank,
                                'no_rek' => $no_rek,
                                'date' => $date,
                                'images' => $images,
                                'category' => $category
                            );
                        $this->M_payment->insert($data);
                        $this->response(['kode' => 1,'pesan' =>'Data berhasil disimpan!'], REST_Controller::HTTP_OK);
                    }
                }else{
                    $this->response([
                        'kode' => 0,
                        'pesan' =>'Data kosong!'
                    ], REST_Controller::HTTP_OK);
                    
                }
        
    }
}