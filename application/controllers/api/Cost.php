<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Cost extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_cost');
    }

    function index_get(){
        $cost=$this->M_cost->get_all();
        $this->response([
                'kode' => 1,
                'result' => $cost,
                'pesan' =>'Data tidak kosong!'
            ], REST_Controller::HTTP_OK);
    }
    
}