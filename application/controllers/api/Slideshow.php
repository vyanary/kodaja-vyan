<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Slideshow extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_slideshow');
    }

    function index_get(){
        $slideshow=$this->M_slideshow->get_by_kat('android');
        $this->response([
            'kode' => 1,
            'result' => $slideshow,
            'pesan' =>'Data tidak kosong!'
        ], REST_Controller::HTTP_OK);
    }
    
}