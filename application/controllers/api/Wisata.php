<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Wisata extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_wisata');
    }

    function index_get(){
        $id_wisata = $this->get('id');
        if ($id_wisata == null) {
            $wisata = $this->M_wisata->get_all();
                $this->response([
                    'kode' => 1,
                    'result' => $wisata,
                    'pesan' =>'Data tidak kosong!'
                ], REST_Controller::HTTP_OK);
        }else{
            $wisata_detail = $this->M_wisata->get_by_id($id_wisata);
            $this->response([
                'kode' => 1,
                'id_wisata'=> $wisata_detail->id_wisata,
                'nama_wisata'=> $wisata_detail->nama_wisata,
                'keterangan'=> $wisata_detail->keterangan ,
                'fasilitas'=> $wisata_detail->fasilitas ,
                'harga'=> $wisata_detail->harga,
                'image'=> $wisata_detail->image,
                'date'=> $wisata_detail->date,
                'pesan' =>'Data tidak kosong!'
            ], REST_Controller::HTTP_OK);
        }
    }
}