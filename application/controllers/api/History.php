<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class History extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_history');
        $this->load->model('M_user');
    }

    function index_get(){
        $id_user = $this->input->get_request_header('User-ID', TRUE);
        $token = $this->input->get_request_header('Token', TRUE);
        $id_history = $this->get('id_history');
        $where = array('id_user' => $id_user,);
        $user_detail = $this->M_user->get_by_id($id_user);
        if ($this->M_user->cek_user($where)->num_rows() > 0) {
            if($user_detail->token != $token){
                $this->response(['kode' => 0,'pesan' =>'Anda tidak memiliki akses!'], REST_Controller::HTTP_OK);
            }else{
                if ($id_history == null) {
                    if ($user_detail->kategori=="driver") {
                        $history=$this->M_history->get_all_history_driver($id_user);
                    }elseif ($user_detail->kategori=="customer") {
                        $history=$this->M_history->get_all_history_customers($id_user);
                    }
                    
                    $this->response([
                            'kode' => 1,
                            'result' => $history,
                            'pesan' =>'Data tidak kosong!'
                        ], REST_Controller::HTTP_OK);
                }else{
                    if ($user_detail->kategori=="driver") {
                        $history=$this->M_history->get_history_driver_by_id($id_history);
                    }elseif ($user_detail->kategori=="customer") {
                        $history=$this->M_history->get_history_customers_by_id($id_history);
                    }
                    $this->response([
                        'kode' => 1,
                        'result' =>$history,
                        'pesan' =>'Data tidak kosong!'
                    ], REST_Controller::HTTP_OK);
                }
            }
        }else{
            $this->response([
                'kode' => 0,
                'pesan' =>'Data kosong!'
            ], REST_Controller::HTTP_OK);
            
        }   
    }
    
}