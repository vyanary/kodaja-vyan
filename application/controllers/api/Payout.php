<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Payout extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_payout','M_user'));
    }

    function index_post(){
        $id = $this->post('id');
        $token = $this->post('token');
        $kode = rand(0,1000000);
        $nominal = $this->post('nominal');
        $where = array('id_user' => $id, );
        $user_detail = $this->M_user->get_by_id($id);
        if ($this->M_user->cek_user($where)->num_rows() > 0) {
            if($user_detail->token != $token){
                $this->response(['kode' => 0,'pesan' =>'Anda tidak memiliki akses!'], REST_Controller::HTTP_OK);
            }else{
                $data = array(
                        'user_id' => $id,
                        'nominal' => $nominal,
                    );
                $this->M_payout->insert($data);
                $this->response(['kode' => 1,'pesan' =>'Data berhasil disimpan!'], REST_Controller::HTTP_OK);
            }
        }else{
            $this->response([
                'kode' => 0,
                'pesan' =>'Data kosong!'
            ], REST_Controller::HTTP_OK);
            
        }
    }
}