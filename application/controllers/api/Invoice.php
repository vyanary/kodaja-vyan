<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Invoice extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_invoice','M_user','M_dana'));
    }

    function index_post(){
        $id = $this->post('id');
        $token = $this->post('token');
        $kode = rand(0,1000000);
        $driver_id = $this->post('driver_id');
        $destination = $this->post('destination');
        $origin = $this->post('origin');
        $cost = $this->post('cost');
        $origin = $this->post('origin');
        $wisata_id = $this->post('wisata_id');
        $wisata = $this->post('wisata');
        $category = $this->post('category');
        $create_on=date("Y-m-d H:i:s");
        $where = array('id_user' => $id,);
        $user_detail = $this->M_user->get_by_id($id);
        if ($this->M_user->cek_user($where)->num_rows() > 0) {
            if($user_detail->token != $token){
                $this->response(['kode' => 0,'pesan' =>'Anda tidak memiliki akses!'], REST_Controller::HTTP_OK);
            }else{
                $data = array(
                    'kode'          => $kode,
                    'user_id'       => $id,
                    'driver_id'     => $driver_id,
                    'origin'        => $origin,
                    'destination'   => $destination,
                    'cost'          => $cost,
                    'wisata_id'     => $wisata_id,
                    'wisata'        => $wisata,
                    'status'        => "unpaid",
                    'category'      => $category,
                    'create_on'     => $create_on,
                );
                $this->M_invoice->insert($data);
                if ($this->post('category')=="driver") {
                    $kodaja=$cost*5/100;
                }else{
                    $kodaja=$cost*10/100;
                }
                $data_dana = array(
                    'invoice_id' => $kode,
                    'cost' => $cost,
                    'kodaja' => $kodaja,
                    'adat' => 5000,
                    'driver' => $cost-($kodaja+5000), 
                );
                $this->M_dana->insert($data_dana);
                $this->response(['kode' => 1,'pesan' =>'Data berhasil disimpan!'], REST_Controller::HTTP_OK);
            }
        }else{
            $this->response([
                'kode' => 0,
                'pesan' =>'Data kosong!'
            ], REST_Controller::HTTP_OK);
            
        }
    }
}