<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class News extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_news');
    }

    function index_get(){
        $id_news = $this->get('id');
        if ($id_news == null) {
            $news=$this->M_news->get_all();
            $this->response([
                    'kode' => 1,
                    'result' => $news,
                    'pesan' =>'Data tidak kosong!'
                ], REST_Controller::HTTP_OK);
        }else{
            $news=$this->M_news->get_by_id($id_news);
            $this->response([
                    'kode' => 1,
                    'result' =>$news,
                    'pesan' =>'Data tidak kosong!'
                ], REST_Controller::HTTP_OK);
        }
    }
    
}