<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_pages');
        $this->load->model('M_contact');
    }

	public function index()
	{
		$about = $this->M_pages->get_by_kat_and_id('website',2);
		$data = array(
            'judul' => $about->judul,
            'sub_judul' => $about->sub_judul,
            'isi' => $about->isi,
            'image' => $about->image,
            'page_title' => "About Us",
        );
		$this->load->view('frontend/about', $data);

	}
}
