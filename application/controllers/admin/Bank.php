<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_bank');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$bank = $this->M_bank->get_all();

        $data = array(
            'data_bank' => $bank,
            'page_title' => "Bank"
		);
		$this->load->view('backend/bank/bank',$data);
	}

	public function tambah(){
		$data = array('page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),);
		$this->load->view('backend/bank/tambah_bank',$data);
	}

	public function edit($id){
		$row = $this->M_bank->get_by_id($id);
        if ($row) {
            $data = array(
		'id_bank' => $row->id_bank,
		'nama_bank' => $row->nama_bank,
		'atas_nama' => $row->atas_nama,
		'no_rek' => $row->no_rek,
		'image' => $row->image,
		'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
	    );
            $this->load->view('backend/bank/edit_bank', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/bank'));
        }
	}

	function simpan(){
		$config = array(
			'upload_path' => './assets/upload/bank/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
			'remove_space' => TRUE,

		);

		$this->load->library('upload', $config);	
		if ($this->upload->do_upload('nama_file')) {
			$upload_data = $this->upload->data();
			$id_bank = '';
			$nama = $_POST['nama'];
			$atas_nama = $_POST['atas_nama'];
			$norek = $_POST['norek'];
			$file_name = $upload_data['file_name'];

			$data = array(	
				'id_bank'=> $id_bank,
				'nama_bank' => $nama,
				'atas_nama' => $atas_nama,
				'no_rek' => $norek,
				'image' => base_url()."assets/upload/bank/".$file_name,		
				);

			$result = $this->M_bank->insert($data);
			if($result>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/bank');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/bank');
			}
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/bank');
		}
		
	}

	function editaction(){
		$config = array(
			'upload_path' => './assets/upload/bank/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',

		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('nama_file');
		$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_bank' => $this->input->post('id'),
				'nama_bank' => $this->input->post('nama'),
				'atas_nama' => $this->input->post('atas_nama'),
				'no_rek' => $this->input->post('norek'),
				'image' => base_url()."assets/upload/bank/".$upload_data['file_name']
				);
			$row=$this->M_bank->get_by_id($data['id_bank']);
			unlink(str_replace(base_url(), "", $row->image));
			$res = $this->M_bank->update($data['id_bank'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/bank');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/bank');
			}		
		}else{
			
			$data = array(
				'id_bank' => $this->input->post('id'),
				'nama_bank' => $this->input->post('nama'),
				'atas_nama' => $this->input->post('atas_nama'),
				'no_rek' => $this->input->post('norek')
				);

			$res = $this->M_bank->update($data['id_bank'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/bank');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/bank');
			}		
		}
	}

	function hapus($kode = 1){
		$row=$this->M_bank->get_by_id($kode);
		unlink(str_replace(base_url(), "", $row->image));
		$result = $this->M_bank->delete($kode);

		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/bank');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/bank');
		}	
	}
}
