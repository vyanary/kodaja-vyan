<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_contact');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$contact = $this->M_contact->get_all();

        $data = array(
            'id_contact' => $contact->id_contact,
			'alamat' => $contact->alamat,
            'contact_person' => $contact->contact_person,
            'legal_information' => $contact->legal_information,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/pages/contact',$data);
	}

	function editaction(){
		$data = array(
			'id_contact' => $this->input->post('id'),
			'alamat' => $this->input->post('alamat'),
            'contact_person' => $this->input->post('contact_person'),
            'legal_information' => $this->input->post('legal_information'),
			);

		$res = $this->M_contact->update($data['id_contact'],$data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/contact');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/contact');
		}		
	}
}
