<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_payment');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}
		
		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$payment = $this->M_payment->get_all();

        $data = array(
            'data_payment' => $payment,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/payment/payment', $data);
	}
}
