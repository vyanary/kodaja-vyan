<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_cost');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$cost = $this->M_cost->get_all();

        $data = array(
            'data_cost' => $cost,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/cost/cost',$data);
	}

	public function tambah(){
		$data = array('page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),);
		$this->load->view('backend/cost/tambah_cost',$data);
	}

	public function edit($id){
		$row = $this->M_cost->get_by_id($id);
        if ($row) {
            $data = array(
		'id_cost' => $row->id_cost,
		'price' => $row->price,
		'kategori' => $row->kategori,
		'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
	    );
            $this->load->view('backend/cost/edit_cost', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/cost'));
        }
	}

	function simpan(){
		$id_cost = '';
		$price = $_POST['price'];
		$kategori = $_POST['kategori'];

		$data = array(	
			'id_cost'=> $id_cost,
			'price' => $price,
			'kategori' => $kategori,
			);

		$result = $this->M_cost->insert($data);
		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/cost');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/cost');
		}
		
	}

	function editaction(){
		$data = array(
			'id_cost' => $this->input->post('id'),
			'price' => $this->input->post('price'),
			'kategori' => $this->input->post('kategori'),
			);

		$res = $this->M_cost->update($data['id_cost'],$data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/cost');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/cost');
		}		
	}

	function hapus($kode = 1){
		$result = $this->M_cost->delete($kode);
		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/cost');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/cost');
		}	
	}
}
