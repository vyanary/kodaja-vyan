<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wisata extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_wisata');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
		
    }

	public function index(){
		$wisata = $this->M_wisata->get_all();

        $data = array(
            'data_wisata' => $wisata,
            'page_title' => ucwords($this->uri->segment(2))
        );
		$this->load->view('backend/wisata/wisata', $data);
	}

	public function tambah(){
		$data = array('page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),);
		$this->load->view('backend/wisata/tambah_wisata',$data);
	}

	public function edit($id){
		$row = $this->M_wisata->get_by_id($id);
        if ($row) {
            $data = array(
				'id_wisata' => $row->id_wisata,
				'nama_wisata' => $row->nama_wisata,
				'keterangan' => $row->keterangan,
				'image' => $row->image,
				'fasilitas' => $row->fasilitas,
				'harga' => $row->harga,
				'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
			);
            $this->load->view('backend/wisata/edit_wisata', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/wisata'));
        }
	}

	function simpan(){
		$config = array(
			'upload_path' => './assets/upload/wisata/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
			'remove_space' => TRUE,

		);

		$this->load->library('upload', $config);	
		if ($this->upload->do_upload('nama_file')) {
			$upload_data = $this->upload->data();
			$id_wisata = '';
			$nama_wisata = $_POST['nama_wisata'];
			$keterangan = $_POST['keterangan'];
			$fasilitas = $_POST['fasilitas'];
			$harga = $_POST['harga'];
			$tanggal = date("Y/m/d");
			$config['image_library']='gd2';
			$config['source_image']='./assets/upload/wisata/'.$upload_data['file_name'];
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '50%';
			$config['width']= 515;
			$config['height']= 322;
			$config['new_image']= './assets/upload/wisata/'.$upload_data['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			$file_name = $upload_data['file_name'];

			$data = array(	
				'id_wisata'=> $id_wisata,
				'nama_wisata' => $nama_wisata,
				'keterangan' => $keterangan,
				'image' => base_url()."assets/upload/wisata/".$file_name,
				'fasilitas' => $fasilitas,	
				'harga' => $harga,
				'date' => $tanggal,	
			);

			$result = $this->M_wisata->insert($data);
			if($result>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/wisata');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/wisata');
			}		
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Upload Gambar GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/wisata');
		}
	}

	function editaction(){
		$config = array(
			'upload_path' => './assets/upload/wisata/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',

		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('nama_file');
		$upload_data = $this->upload->data();
		$config['image_library']='gd2';
		$config['source_image']='./assets/upload/wisata/'.$upload_data['file_name'];
		$config['create_thumb']= FALSE;
		$config['maintain_ratio']= FALSE;
		$config['quality']= '100%';
		$config['width']= 515;
		$config['height']= 322;
		$config['new_image']= './assets/upload/wisata/'.$upload_data['file_name'];
		$this->load->library('image_lib', $config);
		$this->image_lib->resize();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_wisata' => $this->input->post('id'),
				'nama_wisata' => $this->input->post('nama_wisata'),
				'keterangan' => $this->input->post('keterangan'),
				'fasilitas' => $this->input->post('fasilitas'),
				'harga' => $this->input->post('harga'),
				'image' => base_url()."assets/upload/wisata/".$upload_data['file_name']
				);
			$row = $this->M_wisata->get_by_id($data['id_wisata']);
			unlink(str_replace(base_url(), "", $row->image));
			$res = $this->M_wisata->update($data['id_wisata'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/wisata');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/wisata');
			}		
		}else{
			
			$data = array(
				'id_wisata' => $this->input->post('id'),
				'nama_wisata' => $this->input->post('nama_wisata'),
				'keterangan' => $this->input->post('keterangan'),
				'fasilitas' => $this->input->post('fasilitas'),
				'harga' => $this->input->post('harga'),
			);

			$res = $this->M_wisata->update($data['id_wisata'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/wisata');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/wisata');
			}		
		}
	}

	function hapus($kode = 1){
		$row=$this->M_wisata->get_by_id($kode);
		unlink(str_replace(base_url(), "", $row->image));
		$result = $this->M_wisata->delete($kode);

		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/wisata');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/wisata');
		}	
	}
}
