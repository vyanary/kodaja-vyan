<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slideshow extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_slideshow');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$slideshow = $this->M_slideshow->get_all();

        $data = array(
            'data_slideshow' => $slideshow,
            'page_title' => ucwords($this->uri->segment(2))
        );
		$this->load->view('backend/slideshow/slideshow', $data);
	}

	public function tambah(){
		$data = array('page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),);
		$this->load->view('backend/slideshow/tambah_slideshow',$data);
	}

	public function edit($id){
		$row = $this->M_slideshow->get_by_id($id);
        if ($row) {
            $data = array(
				'id_slide' => $row->id_slide,
				'nama_slide' => $row->nama_slide,
				'text' => $row->text,
				'image' => $row->image,
				'kategori' => $row->kategori,
				'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
			);
            $this->load->view('backend/slideshow/edit_slideshow', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/slideshow'));
        }
	}

	function simpan(){
		$config = array(
			'upload_path' => './assets/upload/slideshow/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
			'remove_space' => TRUE,

		);

		$this->load->library('upload', $config);	
		if ($this->upload->do_upload('nama_file')) {
			$upload_data = $this->upload->data();
			$id_slide = '';
			$nama_slide = $_POST['nama_slide'];
			$text = $_POST['text'];
			$kategori = $_POST['kategori'];
			$config['image_library']='gd2';
			$config['source_image']='./assets/upload/slideshow/'.$upload_data['file_name'];
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '50%';
			if ($kategori=="website") {
				$config['width']= 1740;
				$config['height']= 900;
			}else{
				$config['width']= 500;
				$config['height']= 250;
			}
			$config['new_image']= './assets/upload/slideshow/'.$upload_data['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			$file_name = $upload_data['file_name'];

			$data = array(	
				'id_slide'=> $id_slide,
				'nama_slide' => $nama_slide,
				'text' => $text,
				'image' => base_url()."assets/upload/slideshow/".$file_name,
				'kategori' => $kategori,		
			);

			$result = $this->M_slideshow->insert($data);
			if($result>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/slideshow');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/slideshow');
			}
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/slideshow');
		}		
	}

	function editaction(){
		$config = array(
			'upload_path' => './assets/upload/slideshow/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',

		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('nama_file');
		$upload_data = $this->upload->data();
		$config['image_library']='gd2';
		$config['source_image']='./assets/upload/slideshow/'.$upload_data['file_name'];
		$config['create_thumb']= FALSE;
		$config['maintain_ratio']= FALSE;
		$config['quality']= '50%';
		if ($this->input->post('kategori')=="website") {
			$config['width']= 1740;
			$config['height']= 900;
		}else{
			$config['width']= 500;
			$config['height']= 250;
		}
		$config['new_image']= './assets/upload/slideshow/'.$upload_data['file_name'];
		$this->load->library('image_lib', $config);
		$this->image_lib->resize();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_slide' => $this->input->post('id'),
				'nama_slide' => $this->input->post('nama_slide'),
				'text' => $this->input->post('text'),
				'kategori' => $this->input->post('kategori'),
				'image' => base_url()."assets/upload/slideshow/".$upload_data['file_name']
				);
			$row = $this->M_slideshow->get_by_id($data['id_slide']);
			unlink(str_replace(base_url(), "", $row->image));
			$res = $this->M_slideshow->update($data['id_slide'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/slideshow');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/slideshow');
			}		
		}else{
			
			$data = array(
				'id_slide' => $this->input->post('id'),
				'nama_slide' => $this->input->post('nama_slide'),
				'text' => $this->input->post('text'),
				'kategori' => $this->input->post('kategori'),
			);

			$res = $this->M_slideshow->update($data['id_slide'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/slideshow');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/slideshow');
			}		
		}
	}

	function hapus($kode = 1){
		$row=$this->M_slideshow->get_by_id($kode);
		unlink(str_replace(base_url(), "", $row->image));
		$result = $this->M_slideshow->delete($kode);

		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/slideshow');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/slideshow');
		}	
	}
}
