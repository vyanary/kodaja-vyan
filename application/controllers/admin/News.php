<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_news');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$news = $this->M_news->get_all();

        $data = array(
            'data_news' => $news,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/news/news', $data);
	}

	public function tambah(){
		$data = array('page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),);
		$this->load->view('backend/news/tambah_news',$data);
	}

	public function edit($id){
		$row = $this->M_news->get_by_id($id);
        if ($row) {
            $data = array(
				'id_news' => $row->id_news,
				'judul' => $row->judul,
				'isi' => $row->isi,
				'image' => $row->image,
				'tanggal' => $row->tanggal,
				'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
			);
            $this->load->view('backend/news/edit_news', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/news'));
        }
	}

	function simpan(){
		$config = array(
			'upload_path' => './assets/upload/news/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
			'remove_space' => TRUE,

		);

		$this->load->library('upload', $config);	
		if ($this->upload->do_upload('nama_file')) {
			$upload_data = $this->upload->data();
			$id_news = '';
			$judul = $_POST['judul'];
			$isi = $_POST['isi'];
			$tanggal = date("Y/m/d");
			$file_name = $upload_data['file_name'];

			$data = array(	
				'id_news'=> $id_news,
				'judul' => $judul,
				'isi' => $isi,
				'image' => base_url()."assets/upload/news/".$file_name,
				'tanggal' => $tanggal,		
			);

			$result = $this->M_news->insert($data);
			if($result>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/news');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/news');
			}
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/news');
		}		
	}

	function editaction(){
		$config = array(
			'upload_path' => './assets/upload/news/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',

		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('nama_file');
		$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_news' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'isi' => $this->input->post('isi'),
				'image' => base_url()."assets/upload/news/".$upload_data['file_name']
				);
			$row = $this->M_news->get_by_id($data['id_news']);
			unlink(str_replace(base_url(), "", $row->image));
			$res = $this->M_news->update($data['id_news'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/news');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/news');
			}		
		}else{
			
			$data = array(
				'id_news' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'isi' => $this->input->post('isi'),
				);

			$res = $this->M_news->update($data['id_news'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/news');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/news');
			}		
		}
	}

	function hapus($kode = 1){
		$row=$this->M_news->get_by_id($kode);
		unlink(str_replace(base_url(), "", $row->image));
		$result = $this->M_news->delete($kode);

		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/news');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/news');
		}	
	}
}
