<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Awal extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_user');
        $this->load->model('M_payout');
        $this->load->model('M_payment');
        $this->load->model('M_invoice');
    }

	public function index()
	{
		if ($this->session->userdata('status_login')!="login") {
			$data = array('page_title' => "Login",);
			$this->load->view('backend/login',$data);
		}else{
			$data = array(
				'page_title' => "Dashboard",
				'jml_user'=> $this->M_user->total_rows(),
				'jml_invoice'=> $this->M_invoice->total_rows(),
				'jml_payment'=> $this->M_payment->total_rows(),
				'jml_payout'=> $this->M_payout->total_rows(),
				'nama' => ucwords($this->session->userdata('nama')),
			);
			$this->load->view('backend/dashboard',$data);
		}
	}

	public function aksilogin(){
		$email = $this->input->post('email');
		$password=$this->input->post('password');
		$where = array(
			'email' => $email,
			);
		$cek_fase_1 = $this->M_user->cek_login($where)->num_rows();
		$cek_fase_2 = $this->M_user->cek_login($where)->row();
		if($cek_fase_1 > 0){
			$this->load->library('encrypt'); 
			$key = 'vyanarypratamabanyuwangi12345678';
			$password_encrypt =  $this->encrypt->decode($cek_fase_2->password, $key);
			if ($password==$password_encrypt) {
				$data_session = array(
					'id_user' => $cek_fase_2->id_user,
					'nama' => $cek_fase_2->nama,
					'alamat' => $cek_fase_2->alamat,
					'tgl_lahir' => $cek_fase_2->tgl_lahir,
					'email' => $cek_fase_2->email,
					'no_hp' => $cek_fase_2->no_hp,
					'sim' => $cek_fase_2->sim,
					'foto_user' => $cek_fase_2->foto_user,
					'ket' => $cek_fase_2->ket,
					'kategori' => $cek_fase_2->kategori,
					'date' => $cek_fase_2->date,
					'status_login' => "login"
				);
		 
				$this->session->set_userdata($data_session);
		 
				redirect(base_url("admin"));
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Login Gagal </strong> Email dan password anda tidak valid !!</div>");
				redirect(base_url("admin"));
			}
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Login Gagal </strong> Email dan password anda tidak valid !!</div>");
			redirect(base_url("admin"));
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}
}
