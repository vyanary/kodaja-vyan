<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_invoice');
        $this->load->model('M_user');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}
		
		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$invoice = $this->M_invoice->get_all();
        

        $data = array(
            'data_invoice' => $invoice,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/invoice/invoice', $data);
	}

	public function detail($kode){
		$invoice = $this->M_invoice->get_by_id($kode);
		$driver = $this->M_user->get_by_id($invoice->driver_id);

        $data = array(
            'kode' => $invoice->kode,
            'nama_customer' => $invoice->nama,
            'alamat_customer' => $invoice->alamat,
            'hp_customer' => $invoice->no_hp,
            'email_customer' => $invoice->email,
            'nama_driver' => $driver->nama,
            'alamat_driver' => $driver->alamat,
            'hp_driver' => $driver->no_hp,
            'email_driver' => $driver->email,
            'origin' => $invoice->origin,
            'destination' => $invoice->destination,
            'category' => $invoice->category,
            'nama_wisata' => $invoice->nama_wisata,
            'wisata' => $invoice->wisata,
            'harga' => $invoice->harga,
            'cost' => $invoice->cost,
            'create_on' => $invoice->create_on,
            'status' => ucwords($invoice->status),
            'page_title' => ucwords($this->uri->segment(2)." ".$invoice->kode),
        );
		$this->load->view('backend/invoice/detail', $data);
	}

	public function cetak($kode)
	{
		$invoice = $this->M_invoice->get_by_id($kode);
		$driver = $this->M_user->get_by_id($invoice->driver_id);

        $data = array(
            'kode' => $invoice->kode,
            'nama_customer' => $invoice->nama,
            'alamat_customer' => $invoice->alamat,
            'hp_customer' => $invoice->no_hp,
            'email_customer' => $invoice->email,
            'nama_driver' => $driver->nama,
            'alamat_driver' => $driver->alamat,
            'hp_driver' => $driver->no_hp,
            'email_driver' => $driver->email,
            'origin' => $invoice->origin,
            'destination' => $invoice->destination,
            'category' => $invoice->category,
            'nama_wisata' => $invoice->nama_wisata,
            'wisata' => $invoice->wisata,
            'harga' => $invoice->harga,
            'cost' => $invoice->cost,
            'create_on' => $invoice->create_on,
            'status' => ucwords($invoice->status),
            'page_title' => ucwords($this->uri->segment(2)." ".$invoice->kode),
        );
		$this->load->view('backend/invoice/cetak/detail_invoice', $data);
	}
}
