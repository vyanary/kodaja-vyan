<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_pages');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$pages = $this->M_pages->get_all();

        $data = array(
            'data_pages' => $pages,
            'page_title' => ucwords($this->uri->segment(2))
        );
		$this->load->view('backend/pages/pages', $data);
	}

	public function tambah(){
		$data = array('page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),);
		$this->load->view('backend/pages/tambah_pages',$data);
	}

	public function edit($id){
		$row = $this->M_pages->get_by_id($id);
        if ($row) {
            $data = array(
				'id_pages' => $row->id_pages,
				'judul' => $row->judul,
				'sub_judul' => $row->sub_judul,
				'isi' => $row->isi,
				'image' => $row->image,
				'tanggal' => $row->tanggal,
				'kategori' => $row->kategori,
				'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
			);
            $this->load->view('backend/pages/edit_pages', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/pages'));
        }
	}

	function simpan(){
		$config = array(
			'upload_path' => './assets/upload/pages/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
			'remove_space' => TRUE,

		);

		$this->load->library('upload', $config);	
		if ($this->upload->do_upload('nama_file')) {
			$upload_data = $this->upload->data();
			$id_pages = '';
			$judul = $_POST['judul'];
			$sub_judul = $_POST['sub_judul'];
			$isi = $_POST['isi'];
			$kategori = $_POST['kategori'];
			$tanggal = date("Y/m/d");
			$file_name = $upload_data['file_name'];


			$data = array(	
				'id_pages'=> $id_pages,
				'judul' => $judul,
				'sub_judul' => $sub_judul,
				'isi' => $isi,
				'image' => base_url()."assets/upload/pages/".$file_name,
				'tanggal' => $tanggal,
				'kategori' => $kategori,	
			);

			$result = $this->M_pages->insert($data);
			if($result>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/pages');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/pages');
			}
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/pages');
		}		
	}

	function editaction(){
		$config = array(
			'upload_path' => './assets/upload/pages/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',

		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('nama_file');
		$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			
			$data = array(
				'id_pages' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'sub_judul' => $this->input->post('sub_judul'),
				'isi' => $this->input->post('isi'),
				'kategori' => $this->input->post('kategori'),
				'image' => base_url()."assets/upload/pages/".$upload_data['file_name']
				);
			$row = $this->M_pages->get_by_id($data['id_pages']);
			unlink(str_replace(base_url(), "", $row->image));
			$res = $this->M_pages->update($data['id_pages'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/pages');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/pages');
			}		
		}else{
			
			$data = array(
				'id_pages' => $this->input->post('id'),
				'judul' => $this->input->post('judul'),
				'sub_judul' => $this->input->post('sub_judul'),
				'isi' => $this->input->post('isi'),
				'kategori' => $this->input->post('kategori'),
				);

			$res = $this->M_pages->update($data['id_pages'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/pages');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/pages');
			}		
		}
	}

	function hapus($kode = 1){
		$row=$this->M_pages->get_by_id($kode);
		unlink(str_replace(base_url(), "", $row->image));
		$result = $this->M_pages->delete($kode);

		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/pages');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/pages');
		}	
	}
}
