<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payout extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_payout');
        $this->load->model('M_user');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}
		
		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$payout = $this->M_payout->get_all();

        $data = array(
            'data_payout' => $payout,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/payout/payout', $data);
	}

	public function active($id_payout){
		$data_payout = $this->M_payout->get_by_id($id_payout);
		$data_user=$this->M_user->get_by_id($data_payout->user_id);
		$saldo=$data_user->saldo-$data_payout->nominal;
		$data_saldo = array('saldo' => $saldo,);
		$data_payout = array('status' => "approve",);
		$this->M_user->update($id_user,$data_saldo);
		$res = $this->M_payout->update($id_payout,$data_payout);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Payout BERHASIL diapprove</strong></div>");
			header('location:'.base_url().'admin/payout');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Payout GAGAL diapprove</strong></div>");
			header('location:'.base_url().'admin/payout');
		}		
	}
}
