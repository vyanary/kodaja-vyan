<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dana extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_dana');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}
		
		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$dana = $this->M_dana->get_all();

        $data = array(
            'data_dana' => $dana,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/dana/dana', $data);
	}
}
