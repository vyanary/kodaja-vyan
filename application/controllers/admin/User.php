<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_user');
        $this->load->model('M_kendaraan');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}
		

		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$user = $this->M_user->get_where("id_user != ".$this->session->userdata('id_user'));

        $data = array(
            'data_user' => $user,
            'page_title' => ucwords($this->uri->segment(2))
        );
		$this->load->view('backend/user/user', $data);
	}

	public function tambah(){
		$data = array('page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),);
		$this->load->view('backend/user/tambah_user',$data);
	}

	public function edit($id){
		$row = $this->M_user->get_by_id($id);
        if ($row) {
    		$data = array(
				'id_user' => $row->id_user,
				'nama' => $row->nama,
				'alamat' => $row->alamat,
				'tgl_lahir' => $row->tgl_lahir,
				'email' => $row->email,
				'no_hp' => $row->no_hp,
				'sim' => $row->sim,
				'foto_user' => $row->foto_user,
				'ket' => $row->ket,
				'kategori' => $row->kategori,
				'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
			);
            $this->load->view('backend/user/edit_user', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/user'));
        }
	}

	public function profile(){
		$row = $this->M_user->get_by_id($this->session->userdata('id_user'));
		$this->load->library('encrypt'); 
		$key = 'vyanarypratamabanyuwangi12345678';
		$password =  $this->encrypt->decode($row->password, $key);
        if ($row) {
    		$data = array(
				'id_user' => $row->id_user,
				'nama' => $row->nama,
				'alamat' => $row->alamat,
				'tgl_lahir' => $row->tgl_lahir,
				'email' => $row->email,
				'password' => $password,
				'no_hp' => $row->no_hp,
				'sim' => $row->sim,
				'foto_user' => $row->foto_user,
				'ket' => $row->ket,
				'kategori' => $row->kategori,
				'page_title' => ucwords($this->uri->segment(3)." ".$this->uri->segment(2)),
			);
            $this->load->view('backend/user/profile', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/user'));
        }
	}

	function simpan(){
		$config = array(
			'upload_path' => './assets/upload/user/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
			'remove_space' => TRUE,
		);

		$this->load->library('upload', $config);	
		if ($this->upload->do_upload('nama_file')) {
			$this->load->library('encrypt'); 
			$key = 'vyanarypratamabanyuwangi12345678';
			$upload_data = $this->upload->data();
			$id_user = '';
			$nama = $_POST['nama'];
			$alamat = $_POST['alamat'];
			$tgl_lahir = $_POST['tgl_lahir'];
			$email = $_POST['email'];
			$password =  $this->encrypt->encode($_POST['password'], $key);
			$no_hp = $_POST['no_hp'];
			$status = "active";
			$sim = $_POST['sim'];
			$kategori = $_POST['kategori'];
			$ket = $_POST['ket'];
			$tanggal = date("Y/m/d");
			
			$file_name = $upload_data['file_name'];
			if($kategori=="driver"){
				$sim = $_POST['sim'];
			}else{
				$sim = "";
			}
			$data_user = array(	
				'id_user' => $id_user,
				'nama' => $nama,
				'alamat' => $alamat,
				'tgl_lahir' => $tgl_lahir,
				'email' => $email,
				'password' => $password,
				'no_hp' => $no_hp,
				'status' => $status,
				'sim' => $sim,
				'kategori' => $kategori,
				'foto_user' => base_url()."assets/upload/user/".$file_name,
				'ket' => $ket,
				'date' => $tanggal,
				'saldo' => 0,
			);

			$result = $this->M_user->insert($data_user);
			if($result>=0){
				if ($kategori=="driver") {
					$config = array(
						'upload_path' => './assets/upload/kendaraan/',
						'allowed_types' => 'gif|jpg|JPG|png|jpeg',
						'max_size' => '2048',
						'remove_space' => TRUE,
					);
					$this->upload->initialize($config);

					$jml_foto=sizeof($_FILES['nama_file_kendaraan']['tmp_name']);
					$filefoto=$_FILES['nama_file_kendaraan'];

					$nama_foto="";
					
					for($i=0;$i<$jml_foto;$i++){	
						$_FILES['nama_file_kendaraan']['name']=$filefoto['name'][$i];
						$_FILES['nama_file_kendaraan']['type']=$filefoto['type'][$i];
						$_FILES['nama_file_kendaraan']['tmp_name']=$filefoto['tmp_name'][$i];
						$_FILES['nama_file_kendaraan']['error']=$filefoto['error'][$i];
						$_FILES['nama_file_kendaraan']['size']=$filefoto['size'][$i];
						if ($this->upload->do_upload('nama_file_kendaraan')) {
							$upload_data = $this->upload->data();
						}else{
							$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Upload Gambar GAGAL di lakukan</strong></div>");
							header('location:'.base_url().'admin/user');
						}

						$nama_foto=$nama_foto."".$upload_data['file_name']." ";
					}

					$qryuser_terakhir = $this->db->query("SELECT MAX(id_user) as id_terakhir FROM tb_user"); 
	  				$data_user_terakhir = $qryuser_terakhir->row_array();
					$id_kendaraan ='';
					$merk=$_POST['merk'];
					$model=$_POST['model'];
					$warna=$_POST['warna'];
					$tahun=$_POST['tahun'];
					$plat=$_POST['plat'];
					$wilayah=$_POST['wilayah'];
					$foto=$nama_foto;
					$user_id=$data_user_terakhir['id_terakhir'];

					$data_kendaraan = array(	
						'id_kendaraan' => $id_kendaraan,
						'merk' => $merk,
						'model' => $model,
						'warna' => $warna,
						'tahun' => $tahun,
						'plat' => $plat,
						'wilayah' => $wilayah,
						'foto_kendaraan' => $foto,
						'user_id' => $user_id,
					);
					$result_kendaraan = $this->M_kendaraan->insert($data_kendaraan);
					if ($result_kendaraan>=0) {
						$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
						header('location:'.base_url().'admin/user');
					}else{
						$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
						header('location:'.base_url().'admin/user');
					}
				}else{
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'admin/user');
				}
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user');
			}		
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> Upload Gambar User GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/user');
		}
	}

	function editaction(){
		$config = array(
			'upload_path' => './assets/upload/user/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('nama_file');
		$upload_data = $this->upload->data();

		if ($upload_data['file_name'] != null) {
			if ($this->input->post('kategori')=="driver") {
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'no_hp' => $this->input->post('no_hp'),
					'sim' => $this->input->post('sim'),
					'foto' => base_url()."assets/upload/user/".$upload_data['file_name'],
					'ket' => $row->ket,
					'kategori' => $this->input->post('kategori'),
				);
			}else{
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'no_hp' => $this->input->post('no_hp'),
					'foto' => base_url()."assets/upload/user/".$upload_data['file_name'],
					'ket' => $row->ket,
					'kategori' => $this->input->post('kategori'),
				);
			}
			$row = $this->M_user->get_by_id($data['id_user']);
			unlink(str_replace(base_url(), "", $row->foto));
			$res = $this->M_user->update($data['id_user'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/user');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user');
			}		
		}else{
			if ($this->input->post('kategori')=="driver") {
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'no_hp' => $this->input->post('no_hp'),
					'sim' => $this->input->post('sim'),
					'ket' => $row->ket,
					'kategori' => $this->input->post('kategori'),
				);
			}else{
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'no_hp' => $this->input->post('no_hp'),
					'ket' => $row->ket,
					'kategori' => $this->input->post('kategori'),
				);
			}

			$res = $this->M_user->update($data['id_user'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/user');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user');
			}		
		}
	}

	function hapus($kode = 1){
		$row=$this->M_user->get_by_id($kode);
		unlink(str_replace(base_url(), "", $row->foto_user));
		$result = $this->M_user->delete($kode);
		if ($row->kategori=="driver") {
			$row_kendaraan=$this->M_kendaraan->get_by_userid($kode);
			$foto_kendaraan=explode(" ", $row_kendaraan->foto_kendaraan);
			for ($i=0; $i < count($foto_kendaraan)-1; $i++) { 
				unlink("assets/upload/kendaraan/".$foto_kendaraan[$i]);
			}
			$this->M_kendaraan->delete_by_userid($kode);
		}

		if($result>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'admin/user');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/user');
		}	
	}

	function deactive($kode = 1){
		$data = array(
			'status' => "deactive",
		);

		$res = $this->M_user->update($kode,$data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> User berhasil dinonaktikan</strong></div>");
			header('location:'.base_url().'admin/user');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> User gagal dinonaktikan</strong></div>");
			header('location:'.base_url().'admin/user');
		}		
	}

	function active($kode = 1){
		$data = array(
			'status' => "active",
		);

		$res = $this->M_user->update($kode,$data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> User berhasil diaktifkan</strong></div>");
			header('location:'.base_url().'admin/user');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> User gagal diaktifkan</strong></div>");
			header('location:'.base_url().'admin/user');
		}		
	}

	function kendaraan($userid){
		$row = $this->M_kendaraan->get_by_userid($userid);
        if ($row) {
    		$data = array(
				'id_kendaraan' => $row->id_kendaraan,
				'merk' => $row->merk,
				'model' => $row->model,
				'warna' => $row->warna,
				'tahun' => $row->tahun,
				'plat' => $row->plat,
				'wilayah' => $row->wilayah,
				'foto_kendaraan' => $row->foto_kendaraan,
				'nama' => $row->nama,
				'alamat' => $row->alamat,
				'no_hp' => $row->no_hp,
				'page_title' => ucwords($this->uri->segment(3)),
			);
            $this->load->view('backend/user/kendaraan', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/user'));
        }
	}

	function edit_kendaraan($id){
		$row = $this->M_kendaraan->get_by_id($id);
        if ($row) {
    		$data = array(
				'id_kendaraan' => $row->id_kendaraan,
				'merk' => $row->merk,
				'model' => $row->model,
				'warna' => $row->warna,
				'tahun' => $row->tahun,
				'plat' => $row->plat,
				'wilayah' => $row->wilayah,
				'foto_kendaraan' => $row->foto_kendaraan,
				'page_title' => ucwords(str_replace("_", " ", $this->uri->segment(3))),
			);
            $this->load->view('backend/user/edit_kendaraan', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/user'));
        }
	}	

	function editkendaraanaction(){
		$config = array(
			'upload_path' => './assets/upload/kendaraan/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
			'remove_space' => TRUE,
		);
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$jml_foto=sizeof($_FILES['nama_file_kendaraan']['tmp_name']);
		$filefoto=$_FILES['nama_file_kendaraan'];

		$nama_foto=$this->input->post('foto');
		
		for($i=0;$i<$jml_foto;$i++){	
			$_FILES['nama_file_kendaraan']['name']=$filefoto['name'][$i];
			$_FILES['nama_file_kendaraan']['type']=$filefoto['type'][$i];
			$_FILES['nama_file_kendaraan']['tmp_name']=$filefoto['tmp_name'][$i];
			$_FILES['nama_file_kendaraan']['error']=$filefoto['error'][$i];
			$_FILES['nama_file_kendaraan']['size']=$filefoto['size'][$i];
			if ($this->upload->do_upload('nama_file_kendaraan')) {
				$upload_data = $this->upload->data();
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Simpan data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user');
			}

			$nama_foto=$nama_foto."".$upload_data['file_name']." ";
		}

		if ($upload_data['file_name'] != null) {
			$data = array(
				'id_kendaraan' => $this->input->post('id'),
				'merk' => $this->input->post('merk'),
				'model' => $this->input->post('model'),
				'warna' => $this->input->post('warna'),
				'tahun' => $this->input->post('tahun'),
				'plat' => $this->input->post('plat'),
				'wilayah' => $this->input->post('wilayah'),
				'foto_kendaraan' => $nama_foto,
			);
			$row = $this->M_kendaraan->get_by_id($data['id_kendaraan']);
			$foto_kendaraan=explode(" ", $row->foto_kendaraan);
			for ($i=0; $i < count($foto_kendaraan)-1; $i++) { 
				unlink("assets/upload/kendaraan/".$foto_kendaraan[$i]);
			}
			$res = $this->M_kendaraan->update($data['id_kendaraan'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/user/kendaraan/'.$row->user_id);
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user/kendaraan/'.$row->user_id);
			}		
		}else{
			$data = array(
				'id_kendaraan' => $this->input->post('id'),
				'merk' => $this->input->post('merk'),
				'model' => $this->input->post('model'),
				'warna' => $this->input->post('warna'),
				'tahun' => $this->input->post('tahun'),
				'plat' => $this->input->post('plat'),
				'wilayah' => $this->input->post('wilayah'),
			);

			$res = $this->M_kendaraan->update($data['id_kendaraan'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/user/kendaraan/'.$row->user_id);
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user/kendaraan/'.$row->user_id);
			}		
		}
	}

	public function hapusgambarkendaraan($key){
		$id_kendaraan=$this->input->get('id_kendaraan');
		$row = $this->M_kendaraan->get_by_id("$id_kendaraan");
		$foto_kendaraan=explode(" ", $row->foto_kendaraan);
		unlink("assets/upload/kendaraan/".$foto_kendaraan[$key]);
		unset($foto_kendaraan[$key]);
		$foto_kendaraan_array=implode(" ", $foto_kendaraan);
		$data = array(
			'foto_kendaraan' => $foto_kendaraan_array, 
		);
		$res = $this->M_kendaraan->update($id_kendaraan, $data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'admin/user/edit_kendaraan/'.$id_kendaraan);
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus gambar GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'admin/user/edit_kendaraan/'.$id_kendaraan);
		}
	}

	function editprofile(){
		$config = array(
			'upload_path' => './assets/upload/user/',
			'allowed_types' => 'gif|jpg|JPG|png|jpeg',
			'max_size' => '2048',
		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('nama_file');
		$upload_data = $this->upload->data();
		$row = $this->M_user->get_by_id($this->input->post('id'));
		$this->load->library('encrypt'); 
		$key = 'vyanarypratamabanyuwangi12345678';
		$password =  $this->encrypt->encode($this->input->post('password'), $key);

		if ($upload_data['file_name'] != null) {
			if ($row->kategori=="driver") {
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'password' => $password,
					'no_hp' => $this->input->post('no_hp'),
					'sim' => $this->input->post('sim'),
					'foto' => base_url()."assets/upload/user/".$upload_data['file_name'],
					'ket' => $row->ket,
				);
			}else{
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'password' => $password,
					'no_hp' => $this->input->post('no_hp'),
					'foto' => base_url()."assets/upload/user/".$upload_data['file_name'],
					'ket' => $row->ket,
				);
			}
			$row = $this->M_user->get_by_id($data['id_user']);
			unlink(str_replace(base_url(), "", $row->foto));
			$res = $this->M_user->update($data['id_user'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update profile BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/user/profile');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Update profile GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user/profile');
			}		
		}else{
			if ($row->kategori=="driver") {
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'password' => $password,
					'no_hp' => $this->input->post('no_hp'),
					'sim' => $this->input->post('sim'),
					'ket' => $row->ket,
				);
			}else{
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'email' => $this->input->post('email'),
					'password' => $password,
					'no_hp' => $this->input->post('no_hp'),
					'ket' => $row->ket,
				);
			}

			$res = $this->M_user->update($data['id_user'],$data);
			if($res>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> Update profile BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'admin/user/profile');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i></i> <strong> Update profile GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'admin/user/profile');
			}		
		}
	}
}
