<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topup extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_topup');
        $this->load->model('M_user');
        if ($this->session->userdata('status_login')!="login") {
			redirect(base_url("admin"));
		}
		
		/*if ($this->session->userdata('kategori')!="admin") {
			redirect(base_url("admin"));
		}*/
    }

	public function index(){
		$topup = $this->M_topup->get_all();

        $data = array(
            'data_topup' => $topup,
            'page_title' => ucwords($this->uri->segment(2)),
        );
		$this->load->view('backend/topup/topup', $data);
	}

	public function active($id_user){
		$data_user=$this->M_user->get_by_id($id_user);
		$saldo=$data_user->saldo+$this->input->get('nominal');
		$id_topup=$this->input->get('id_topup');
		$data_saldo = array('saldo' => $saldo,);
		$data_topup = array('status_topup' => "approve",);
		$this->M_user->update($id_user,$data_saldo);
		$res = $this->M_topup->update($id_topup,$data_topup);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><i class='fa fa-check'></i> <strong> TopUp BERHASIL diapprove</strong></div>");
			header('location:'.base_url().'admin/topup');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><i class='fa fa-exclamation'></i> <strong> TopUp GAGAL diapprove</strong></div>");
			header('location:'.base_url().'admin/topup');
		}		
	}
}
