<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_wisata');
        $this->load->model('M_pages');
        $this->load->model('M_contact');
    }

	public function index(){
		$wisata = $this->M_wisata->get_all();
		$pages_wisata = $this->M_pages->get_by_kat_and_id('website',3);
		$data = array(
            'data_wisata' => $wisata,
            'judul' => $pages_wisata->judul,
            'sub_judul' => $pages_wisata->sub_judul,
            'isi' => $pages_wisata->isi,
            'image' => $pages_wisata->image,
            'page_title' => "Paket Wisata",
        );
		$this->load->view('frontend/paket',$data);
	}

	public function detail($id){
		$row = $this->M_wisata->get_by_id($id);
		$wisata = $this->M_wisata->get_where('id_wisata != '.$row->id_wisata);
        if ($row) {
            $data = array(
				'id_wisata' => $row->id_wisata,
				'nama_wisata' => $row->nama_wisata,
				'keterangan' => $row->keterangan,
				'image' => $row->image,
				'fasilitas' => $row->fasilitas,
				'harga' => $row->harga,
				'data_wisata' => $wisata,
				'page_title' => $row->nama_wisata,
			);
            $this->load->view('frontend/detail-paket',$data);
        } else {
        	$this->load->view('frontend/detail-paket');
            $this->session->set_flashdata('message', 'Record Not Found');
        }
		
	}
}
