<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct()    {
        parent::__construct();
        $this->load->model('M_pages');
        $this->load->model('M_contact');
    }

	public function index()
	{
		$contact_pages = $this->M_pages->get_by_kat_and_id('website',4);
		$contact = $this->M_contact->get_all();
		$data= array(
			'judul' => $contact_pages->judul,
            'sub_judul' => $contact_pages->sub_judul,
            'isi' => $contact_pages->isi,
            'image' => $contact_pages->image,
            'alamat' => $contact->alamat,
            'contact_person' => $contact->contact_person,
            'legal_information' => $contact->legal_information,
			'page_title' => "Contact Us", 
		);
		$this->load->view('frontend/contact', $data);
	}
}
