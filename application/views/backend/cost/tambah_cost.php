	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Data Cost
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form role="form" action="<?php echo base_url('admin/cost/simpan'); ?>" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="">Price</label>
                    <input type="text" class="form-control" id="" name="price" placeholder="Isikan Price" required>
                  </div>
                  <div class="form-group">
                    <label for="">Kategori Cost</label>
                    <select class="form-control select2" required name="kategori" style="width: 100%;">
                      <option value="">--Pilih Kategori--</option>
                      <option value="0-3 km">0-3 km</option>
                      <option value=">3 km">>3 km</option>
                      <option value="weekend">Weekend</option>
                    </select>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pastikan semua data telah terisi</h3> 
            </div>
            <div class="box-body">
              <button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-plus"></i> Tambah Data</button>
              <a href="<?php echo base_url('admin/cost') ?>"><span class="btn btn-flat btn-danger"><i class="fa fa-ban"></i> Batal</span></a>
            </div>
          </div>
        </div>
        <!-- /.col -->
        </form>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	  <?php $this->load->view('backend/js'); ?>
    <!-- Select2 -->
    <script src="<?php echo base_url('assets/backend/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
    <script>
      $(function () {
        $('.select2').select2();
      })
    </script>

	</body>
</html>