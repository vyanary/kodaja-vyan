	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Topup
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
	          	<div class="box">
		            <div class="box-body table-responsive">
		              <table id="example1" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Nama User</th>
			                  <th>Nominal</th>
			                  <th>Pembayaran</th>
			                  <th>Status TopUp</th>
			                  <th>Aksi</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php
		                		$start = 0;
		            			foreach ($data_topup as $topup){ 
		                	?>
		                	<tr>
		                		<td><?php echo ++$start ?></td>
		                		<td><?php echo $topup->nama ?></td>
		                		<td><?php echo $topup->nominal ?></td>
		                		<td><?php echo $topup->pembayaran ?></td>
		                		<td>
		                			<span class="label <?php if($topup->status_topup=="approve"){ echo "label-success"; }else{ echo "label-danger"; }?>">	
		                				<?php echo ucwords($topup->status_topup) ?>
		                			</span>
		                		</td>
			                    <td align="center">
			                    	<?php if($topup->status_topup!="approve"){ ?>
			                    	<a title="Approve" data-toggle="modal" style="margin-bottom: 5px" data-target="#modalactive<?php echo $start ?>" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
			                    	<!-- Modal Hapus -->
				                    <div class="modal fade" id="modalactive<?php echo $start ?>">
  							          	<div class="modal-dialog modal-sm">
  							            	<div class="modal-content">
  							              		<div class="modal-header">
  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							                  		<span aria-hidden="true">&times;</span></button>
  							                		<h4 class="modal-title">Peringatan</h4>
  							              		</div>
  							              		<div class="modal-body">
  							                		<p>Yakin Ingin Mengapprove Topup Ini ?</p>
  							              		</div>
  							              		<div class="modal-footer">
  							              			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
  							                		<a href="<?php echo base_url('admin/topup/active/'.$topup->id_user."?id_topup=".$topup->kode."&nominal=".$topup->nominal); ?>" class="btn btn-primary" style="margin-right: 10px;">Yes</a>
  							              		</div>
  							            	</div>
  							          	</div>
  							        </div>
  							       	<?php } ?>
			                    </td>
		                	</tr>
		                	<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>