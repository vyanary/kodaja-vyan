	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<body class="hold-transition login-page">
		<div class="login-box">
		  <div class="login-logo">
		    <a href="<?php echo base_url('admin') ?>"><b>Kodaja </b>Trans Bali</a>
		  </div>
		  <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
		  <!-- /.login-logo -->
		  <div class="login-box-body">
		    <p class="login-box-msg">Sign in please</p>

		    <form action="<?php echo base_url('admin/awal/aksilogin') ?>" method="post">
		      <div class="form-group has-feedback">
		        <input type="email" name="email" class="form-control" placeholder="Email">
		        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		      </div>
		      <div class="form-group has-feedback">
		        <input type="password" name="password" class="form-control" placeholder="Password">
		        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		      </div>
		      <div class="row">
		        <!-- /.col -->
		        <div class="col-xs-4 pull-right">
		          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
		        </div>
		        <!-- /.col -->
		      </div>
		    </form>
		    <hr>
                
            <div class="social-auth-links text-blue">
                Apabila anda lupa dengan password anda, segera hubungi admin.
            </div>
            <div style="text-align:center; font-size:13px; margin-top:30px;">
                <strong>Copyright &copy; 2018 <a href=""> Your Company</a></strong>
            </div>

		  </div>
		  <!-- /.login-box-body -->
		</div>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>