	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data User
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
		        <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
		        <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
	          	<div class="box">
		            <div class="box-header">
		              <a href="<?php echo base_url('admin/user/tambah') ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah User</a>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body table-responsive">
		              <table id="example1" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  	<th>No.</th>
			                  	<th>Nama user</th>
			                  	<th>Email</th>
                        		<th>Alamat</th>
			                  	<th>No. HP</th>
			                  	<th>Kategori</th>
			                  	<th>Foto</th>
			                  	<th>Status</th>
			                  	<th>Aksi</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php
		                		$start = 0;
		            			foreach ($data_user as $user){ 
		                	?>
		                	<tr>
		                		<td><?php echo ++$start ?></td>
		                		<td><?php echo $user->nama ?></td>
			                  	<td><?php echo $user->email ?></td>
			                  	<td><?php echo $user->alamat ?></td>
			                  	<td><?php echo $user->no_hp ?></td>
			                  	<td><?php echo $user->kategori ?></td>
			                  	<td>
		                			<img style="width:80px;" src="<?php echo $user->foto_user; ?>" class="img-thumbnail" alt="User Image" />
			                  	</td>
			                  	<td>
			                  		<span class="label <?php if($user->status=="active"){ echo "label-success"; }else{ echo "label-danger"; }?>"><?php echo $user->status ?></span>
			                  	</td>
			                  	<td align="center">
		                    		<a class="btn btn-warning btn-sm" title="Edit" style="margin-bottom: 5px" href="<?php echo base_url('admin/user/edit/'.$user->id_user); ?>"><i class="fa fa-pencil"></i></a>
		                    		<?php if($user->kategori=="driver"){ ?>
			                    		<?php if($user->status=="active"){ ?>
			                    		<a data-toggle="modal" title="Deactive" style="margin-bottom: 5px" data-target="#modalban<?php echo $start ?>" class="btn btn-danger btn-sm"><i class="fa fa-ban" ></i></a>
				                    	<!-- Modal Hapus -->
					                    <div class="modal fade" id="modalban<?php echo $start ?>">
	  							          	<div class="modal-dialog modal-sm">
	  							            	<div class="modal-content">
	  							              		<div class="modal-header">
	  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  							                  		<span aria-hidden="true">&times;</span></button>
	  							                		<h4 class="modal-title">Peringatan</h4>
	  							              		</div>
	  							              		<div class="modal-body">
	  							                		<p>Yakin Ingin Menonaktifkan Driver Ini ?</p>
	  							              		</div>
	  							              		<div class="modal-footer">
	  							              			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	  							                		<a href="<?php echo base_url('admin/user/deactive/'.$user->id_user); ?>" class="btn btn-primary" style="margin-right: 10px;">Yes</a>
	  							              		</div>
	  							            	</div>
	  							          	</div>
	  							        </div>
	  							        <?php }else{ ?>
	  							        <a title="Aktifkan" data-toggle="modal" style="margin-bottom: 5px" data-target="#modalactive<?php echo $start ?>" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
				                    	<!-- Modal Hapus -->
					                    <div class="modal fade" id="modalactive<?php echo $start ?>">
	  							          	<div class="modal-dialog modal-sm">
	  							            	<div class="modal-content">
	  							              		<div class="modal-header">
	  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  							                  		<span aria-hidden="true">&times;</span></button>
	  							                		<h4 class="modal-title">Peringatan</h4>
	  							              		</div>
	  							              		<div class="modal-body">
	  							                		<p>Yakin Ingin Mengaktifkan Driver Ini ?</p>
	  							              		</div>
	  							              		<div class="modal-footer">
	  							              			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	  							                		<a href="<?php echo base_url('admin/user/active/'.$user->id_user); ?>" class="btn btn-primary" style="margin-right: 10px;">Yes</a>
	  							              		</div>
	  							            	</div>
	  							          	</div>
	  							        </div>
	  							        <a title="Hapus" data-toggle="modal" style="margin-bottom: 5px" data-target="#modalhapus<?php echo $start ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
				                    	<!-- Modal Hapus -->
					                    <div class="modal fade" id="modalhapus<?php echo $start ?>">
	  							          	<div class="modal-dialog modal-sm">
	  							            	<div class="modal-content">
	  							              		<div class="modal-header">
	  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  							                  		<span aria-hidden="true">&times;</span></button>
	  							                		<h4 class="modal-title">Peringatan</h4>
	  							              		</div>
	  							              		<div class="modal-body">
	  							                		<p>Yakin Ingin menghapus data Ini ?</p>
	  							              		</div>
	  							              		<div class="modal-footer">
	  							              			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	  							                		<a href="<?php echo base_url('admin/user/hapus/'.$user->id_user); ?>" class="btn btn-primary" style="margin-right: 10px;">Yes</a>
	  							              		</div>
	  							            	</div>
	  							          	</div>
	  							        </div>
	  							        <?php } ?>
	  							    <a title="Lihat Kendaraan" class="btn btn-primary btn-sm" style="margin-bottom: 5px" href="<?php echo base_url('admin/user/kendaraan/'.$user->id_user); ?>"><i class="fa fa-car"></i></a>
	  							    <?php }else{  ?>
	  							    <a title="Hapus" data-toggle="modal" style="margin-bottom: 5px" data-target="#modalhapus<?php echo $start ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
			                    	<!-- Modal Hapus -->
				                    <div class="modal fade" id="modalhapus<?php echo $start ?>">
  							          	<div class="modal-dialog modal-sm">
  							            	<div class="modal-content">
  							              		<div class="modal-header">
  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							                  		<span aria-hidden="true">&times;</span></button>
  							                		<h4 class="modal-title">Peringatan</h4>
  							              		</div>
  							              		<div class="modal-body">
  							                		<p>Yakin Ingin menghapus data Ini ?</p>
  							              		</div>
  							              		<div class="modal-footer">
  							              			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
  							                		<a href="<?php echo base_url('admin/user/hapus/'.$user->id_user); ?>" class="btn btn-primary" style="margin-right: 10px;">Yes</a>
  							              		</div>
  							            	</div>
  							          	</div>
  							        </div>
	  							    <?php } ?>
			                  	</td>
		                	</tr>
		                	<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>