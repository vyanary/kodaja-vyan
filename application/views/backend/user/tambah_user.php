	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah User
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form role="form" action="<?php echo base_url('admin/user/simpan'); ?>" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                          <label for="">Kategori User </label>
                          <select class="form-control" id="test" required name="kategori" style="width: 100%;">
                            <option value="">--Pilih Kategori User--</option>
                            <option value="customer">Customer</option>
                            <option value="admin">Admin</option>
                            <option value="driver">Driver</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="">Nama user</label>
                          <input type="text" class="form-control" value="" id="" name="nama" placeholder="Masukkan Nama user" required>
                      </div>
                      <div class="form-group">
                          <label for="">Email</label>
                          <input type="text" class="form-control" value="" id="" name="email" placeholder="Masukkan Email User" required>
                      </div>
                      <div class="form-group">
                          <label for="">Password</label>
                          <input type="password" class="form-control" value="" id="" name="password" placeholder="Masukkan Password User" required>
                      </div>
                      <div class="form-group">
                          <label for="">Alamat</label>
                          <textarea required name="alamat" class="form-control" placeholder="Masukkan Alamat User"></textarea >
                      </div>
                      <div class="form-group">
                          <label>Tanggal Lahir</label>
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="tgl_lahir" id="datepicker">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="">No. Handphone</label>
                          <input type="text" class="form-control" value="" id="" name="no_hp" placeholder="Masukkan No. Handphone User" required>
                      </div>
                      <div class="form-group" id="hidden_div" style="display: none;">
                          <label for="">SIM </label>
                          <select class="form-control" id="test" name="sim" style="width: 100%;">
                            <option value="">--Pilih SIM User--</option>
                            <option value="SIM A">SIM A</option>
                            <option value="SIM A UMUM">SIM A UMUM</option>
                            <option value="SIM B I">SIM B I</option>
                            <option value="SIM B I UMUM">SIM B I UMUM</option>
                            <option value="SIM B II">SIM B II</option>
                            <option value="SIM B II UMUM">SIM B II UMUM</option>
                            <option value="SIM C">SIM C</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="">Keterangan </label>
                          <textarea required name="ket" class="form-control" placeholder="Masukkan Keterangan User"></textarea >
                      </div>
                      <div class="form-group">
                        <label>Foto</label>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <div class="col-md-12" style="border:1px solid #ddd;padding:5px;margin-top: -15px;margin-left:-15px;" align="center">
                              <img src="<?php echo base_url('assets/backend/img/nopicture.jpg') ?>" class="img-responsive" id="preview_foto">
                            </div>
                            <div class="clearfix"></div>
                            <p class="small" style="margin-left:-15px;margin-bottom: 10px;">*Max file size 2MB </p>
                            <div class="col-sm-offset-2" style="margin-left:25%;">
                              <div class="button-upload btn btn-flat btn-primary width-183px">
                                  <span><i class="fa fa-folder-open"></i> Pilih Foto</span>
                                  <input name="nama_file" class="upload" type="file" accept="image/*" required onchange="tampilkanPreview(this,'preview_foto')"/> 
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-xs-12">
          <div class="box" id="hidden_div1" style="display: none;">
            <div class="box-header with-border">
              <h3 class="box-title">Form Kendaraan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-12" > 
                       <div class="form-group">
                          <label for="">Merk Kendaraan</label>
                          <input type="text" class="form-control" value="" id="" name="merk" placeholder="Masukkan Merk Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Model Kendaraan</label>
                          <input type="text" class="form-control" value="" id="" name="model" placeholder="Masukkan Model Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Warna Kendaraan</label>
                          <input type="text" class="form-control" value="" id="" name="warna" placeholder="Masukkan Warna Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Tahun Pembuatan Kendaraan</label>
                          <input type="text" class="form-control" value="" id="" name="tahun" placeholder="Masukkan Tahun Pembuatan Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Plat Kendaraan</label>
                          <input type="text" class="form-control" value="" id="" name="plat" placeholder="Masukkan Plat Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Wilayah Plat Kendaraan</label>
                          <input type="text" class="form-control" value="" id="" name="wilayah" placeholder="Masukkan Wilayah Plat Kendaraan" >
                      </div>
                      <div class="form-group" id="fotokendaraan">
                        <label>Foto Kendaraan 
                          <a href='#' onclick="tambah_form(); return false;" >Add</a>
                          <a href='#' onclick="kurangi_form(); return false;">Remove</a>
                        </label>
                        <input name="nama_file_kendaraan[]" class="form-control" type="file"  /> 
                      </div>
                      <div class="form-group">
                        <p class="small" style="margin-bottom: 10px;">*Max file size 2MB </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pastikan semua data telah terisi</h3> 
            </div>
            <div class="box-body">
              <button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-plus"></i> Tambah Data</button>
              <a href="<?php echo base_url('admin/user') ?>"><span class="btn btn-flat btn-danger"><i class="fa fa-ban"></i> Batal</span></a>
            </div>
          </div>
        </div>
        <!-- /.col -->
        </form>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	  <?php $this->load->view('backend/js'); ?>
    <!-- bootstrap datepicker -->
    <script src="<?php echo base_url('assets/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
    <!-- Preview foto -->
    <script>
      
      function tampilkanPreview(gambar,idpreview){
          var gb = gambar.files;
          
          for (var i = 0; i < gb.length; i++){
              var gbPreview = gb[i];
              var imageType = /image.*/;
              var preview=document.getElementById(idpreview);            
              var reader = new FileReader();
              
              if (gbPreview.type.match(imageType)) {
                  preview.file = gbPreview;
                  reader.onload = (function(element) { 
                      return function(e) { 
                          element.src = e.target.result; 
                      }; 
                  })(preview);

                  reader.readAsDataURL(gbPreview);
              }else{
                  alert("Type file tidak sesuai. Khusus image.");
              }
          }    
      }
    </script>
    <script>
      var select = document.getElementById('test'),
      onChange = function(event) {
          var shown = this.options[this.selectedIndex].value == "driver";

          document.getElementById('hidden_div').style.display = shown ? 'block' : 'none';
          document.getElementById('hidden_div1').style.display = shown ? 'block' : 'none';
      };

      // attach event handler
      if (window.addEventListener) {
          select.addEventListener('change', onChange, false);
      } else {
          // of course, IE < 9 needs special treatment
          select.attachEvent('onchange', function() {
              onChange.apply(select, arguments);
          });
      }
      //Date picker
      $('#datepicker').datepicker({
        autoclose: true
      })
    </script>
    <script>
      function tambah_form(){
          var target=document.getElementById("fotokendaraan");
          var tabel_col=document.createElement("td");
          var tambah=document.createElement("input");
          target.appendChild(tambah);
          tambah.setAttribute('type','file');
          tambah.setAttribute('name','nama_file_kendaraan[]');
          tambah.setAttribute('class','form-control');
          tambah.setAttribute('required');

      }
      function kurangi_form(){
        var target=document.getElementById("fotokendaraan");
        var akhir=target.lastChild;
        target.removeChild(akhir);
      }
    </script>
	</body>
</html>