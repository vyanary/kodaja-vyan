	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Kendaraan
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form role="form" action="<?php echo base_url('admin/user/editkendaraanaction'); ?>" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Kendaraan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-12" > 
                       <div class="form-group">
                          <label for="">Merk Kendaraan</label>
                          <input type="hidden" class="form-control" value="<?php echo $id_kendaraan ?>" id="" name="id" placeholder="Masukkan Merk Kendaraan" >
                          <input type="text" class="form-control" value="<?php echo $merk ?>" id="" name="merk" placeholder="Masukkan Merk Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Model Kendaraan</label>
                          <input type="text" class="form-control" value="<?php echo $model ?>" id="" name="model" placeholder="Masukkan Model Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Warna Kendaraan</label>
                          <input type="text" class="form-control" value="<?php echo $warna ?>" id="" name="warna" placeholder="Masukkan Warna Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Tahun Pembuatan Kendaraan</label>
                          <input type="text" class="form-control" value="<?php echo $tahun ?>" id="" name="tahun" placeholder="Masukkan Tahun Pembuatan Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Plat Kendaraan</label>
                          <input type="text" class="form-control" value="<?php echo $plat ?>" id="" name="plat" placeholder="Masukkan Plat Kendaraan" >
                      </div>
                      <div class="form-group">
                          <label for="">Wilayah Plat Kendaraan</label>
                          <input type="text" class="form-control" value="<?php echo $wilayah ?>" id="" name="wilayah" placeholder="Masukkan Wilayah Plat Kendaraan" >
                      </div>
                      <div class="form-group" id="fotokendaraan">
                        <label>Foto Kendaraan 
                          <a href='#' onclick="tambah_form(); return false;" >Add</a>
                          <a href='#' onclick="kurangi_form(); return false;">Remove</a>
                        </label>
                        <?php 
                          $foto = explode(" ", $foto_kendaraan);
                        ?>
                        <div class="row">
                          <?php for ($i=0; $i < count($foto)-1; $i++) { ?>
                          <div class="col-lg-6" style="margin: 0;padding: 10px">
                            <img src="<?php echo base_url('assets/upload/kendaraan/'.$foto[$i]); ?>" class="img-responsive img-thumbnail">
                            <a href="<?php echo base_url('admin/user/hapusgambarkendaraan/'.$i.'?id_kendaraan='.$id_kendaraan); ?>" >Hapus</a>
                          </div>
                          <?php } ?>
                        </div>
                        <input name="nama_file_kendaraan[]" class="form-control" type="file"  />
                        <input type="hidden" name="foto" value="<?php echo $foto_kendaraan ?>"> 
                      </div>
                      <div class="form-group">
                        <p class="small" style="margin-bottom: 10px;">*Max file size 2MB </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pastikan semua data telah terisi</h3> 
            </div>
            <div class="box-body">
              <button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-save"></i> Simpan Data</button>
              <a href="<?php echo base_url('admin/user') ?>"><span class="btn btn-flat btn-danger"><i class="fa fa-ban"></i> Batal</span></a>
            </div>
          </div>
        </div>
        <!-- /.col -->
        </form>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	  <?php $this->load->view('backend/js'); ?>
    <!-- Preview foto -->
    <script>
      
      function tampilkanPreview(gambar,idpreview){
          var gb = gambar.files;
          
          for (var i = 0; i < gb.length; i++){
              var gbPreview = gb[i];
              var imageType = /image.*/;
              var preview=document.getElementById(idpreview);            
              var reader = new FileReader();
              
              if (gbPreview.type.match(imageType)) {
                  preview.file = gbPreview;
                  reader.onload = (function(element) { 
                      return function(e) { 
                          element.src = e.target.result; 
                      }; 
                  })(preview);

                  reader.readAsDataURL(gbPreview);
              }else{
                  alert("Type file tidak sesuai. Khusus image.");
              }
          }    
      }
    </script>
    <script>
      function tambah_form(){
          var target=document.getElementById("fotokendaraan");
          var tabel_col=document.createElement("td");
          var tambah=document.createElement("input");
          target.appendChild(tambah);
          tambah.setAttribute('type','file');
          tambah.setAttribute('name','nama_file_kendaraan[]');
          tambah.setAttribute('class','form-control');
          tambah.setAttribute('required');

      }
      function kurangi_form(){
        var target=document.getElementById("fotokendaraan");
        var akhir=target.lastChild;
        target.removeChild(akhir);
      }
    </script>
	</body>
</html>