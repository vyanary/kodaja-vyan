	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kendaraan
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
		        <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
		        <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
	          	<div class="box">
		            <div class="box-header">
		              <a href="<?php echo base_url('admin/user/edit_kendaraan/'.$id_kendaraan) ?>" class="btn btn-warning"><span class="fa fa-pencil"></span> Edit Kendaraan</a>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            	<div class="row">
		            		<div class="col-lg-4">
		            			<h4 style="font-weight: bold">Foto Kendaraan</h4>
		            			<hr>
		            			<?php 
		            				$foto = explode(" ", $foto_kendaraan);
		            			?>
		            			<div class="row">
		            				<?php for ($i=0; $i < count($foto)-1; $i++) { ?>
		            				<div class="col-lg-6" style="margin: 0;padding: 10px">
		            					<img src="<?php echo base_url('assets/upload/kendaraan/'.$foto[$i]); ?>" class="img-responsive img-thumbnail">
		            				</div>
		            				<?php } ?>
		            			</div>
		            		</div>
		            		<div class="col-lg-4">
		            			<h4 style="font-weight: bold">Identitas User</h4>
		            			<hr>
		            			<form class="form-horizontal">
								  	<div class="form-group" style="margin-bottom: 0px">
									    <label class="col-sm-5 control-label" style="text-align: left">Nama</label>
									    <div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
									    <div class="col-sm-6">
									      	<p class="form-control-static"><?php echo $nama ?></p>
									    </div>
								  	</div>
								  	<div class="form-group" style="margin-bottom: 0px">
								    	<label class="col-sm-5 control-label" style="text-align: left">Alamat</label>
								    	<div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
								    	<div class="col-sm-6">
								      		<p class="form-control-static"><?php echo $alamat ?></p>
								    	</div>
								  	</div>
								  	<div class="form-group" style="margin-bottom: 0px">
								    	<label class="col-sm-5 control-label" style="text-align: left">No. Handphone</label>
								    	<div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
								    	<div class="col-sm-6">
								      		<p class="form-control-static"><?php echo $no_hp ?></p>
								    	</div>
								  	</div>
								</form>	
		            		</div>
		            		<div class="col-lg-4">
		            			<h4 style="font-weight: bold">Identitas Kendaraan</h4>
		            			<hr>
		            			<form class="form-horizontal">
								  	<div class="form-group" style="margin-bottom: 0px">
									    <label class="col-sm-3 control-label" style="text-align: left">Merk</label>
									    <div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
									    <div class="col-sm-8">
									      	<p class="form-control-static"><?php echo $merk ?></p>
									    </div>
								  	</div>
								  	<div class="form-group" style="margin-bottom: 0px">
								    	<label class="col-sm-3 control-label" style="text-align: left">Model</label>
								    	<div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
								    	<div class="col-sm-8">
								      		<p class="form-control-static"><?php echo $model ?></p>
								    	</div>
								  	</div>
								  	<div class="form-group" style="margin-bottom: 0px">
								    	<label class="col-sm-3 control-label" style="text-align: left">Warna</label>
								    	<div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
								    	<div class="col-sm-8">
								      		<p class="form-control-static"><?php echo $warna ?></p>
								    	</div>
								  	</div>
								  	<div class="form-group" style="margin-bottom: 0px">
								    	<label class="col-sm-3 control-label" style="text-align: left">Tahun</label>
								    	<div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
								    	<div class="col-sm-8">
								      		<p class="form-control-static"><?php echo $tahun ?></p>
								    	</div>
								  	</div>
								  	<div class="form-group" style="margin-bottom: 0px">
								    	<label class="col-sm-3 control-label" style="text-align: left">Plat</label>
								    	<div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
								    	<div class="col-sm-8">
								      		<p class="form-control-static"><?php echo $plat ?></p>
								    	</div>
								  	</div>
								  	<div class="form-group" style="margin-bottom: 0px">
								    	<label class="col-sm-3 control-label" style="text-align: left">Wilayah</label>
								    	<div class="col-sm-1" style="margin-left: -10px;padding-left: 0px;margin-right: -20px;">
									    	<p class="form-control-static">:</p>
									    </div>
								    	<div class="col-sm-8">
								      		<p class="form-control-static"><?php echo $wilayah ?></p>
								    	</div>
								  	</div>
								</form>
		            		</div>
		            	</div>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	</body>
</html>