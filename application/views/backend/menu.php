</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red-default                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-red fixed sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="<?=base_url('admin')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>KDJ</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo strtoupper($this->session->userdata('kategori')); ?></b> KODAJA</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php echo $this->session->userdata('foto_user'); ?>" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo ucwords($this->session->userdata('nama')); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="<?php echo $this->session->userdata('foto_user'); ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo ucwords($this->session->userdata('nama')); ?> - <?php echo ucwords($this->session->userdata('kategori')); ?>
                  <small>Member since <?php echo date("F Y",strtotime(str_replace("/", "-",$this->session->userdata('date')))); ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('admin/user/profile') ?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('admin/awal/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $this->session->userdata('foto_user'); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucwords($this->session->userdata('nama')); ?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="<?php if($this->uri->segment(2)==""){echo "active";}?>">
          <a href="<?php echo base_url('admin') ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="header">Master Data</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="<?php if($this->uri->segment(2)=="bank"){echo "active";}?>">
          <a href="<?php echo base_url('admin/bank') ?>">
            <i class="fa fa-university"></i> <span>Bank</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2)=="slideshow"){echo "class='active'";}?>>
          <a href="<?php echo base_url('admin/slideshow') ?>">
            <i class="fa fa-picture-o "></i> <span>Slideshow</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(2)=="cost"){echo "active";}?>">
          <a href="<?php echo base_url('admin/cost') ?>">
            <i class="fa fa-money"></i> <span>Cost</span>
          </a>
        </li>
        <li class="header">Pages</li>
        <li class="<?php if($this->uri->segment(2)=="news"){echo "active";}?>">
          <a href="<?php echo base_url('admin/news') ?>">
            <i class="fa fa-newspaper-o"></i> <span>News</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2)=="wisata"){echo "class='active'";}?>>
          <a href="<?php echo base_url('admin/wisata') ?>">
            <i class="fa fa-plane"></i> <span>Wisata</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(2)=="pages"){echo "active";}?>">
          <a href="<?php echo base_url('admin/pages') ?>">
            <i class="fa fa-columns"></i> <span>Pages</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(2)=="contact"){echo "active";}?>">
          <a href="<?php echo base_url('admin/contact') ?>">
            <i class="fa fa-phone"></i> <span>Contact</span>
          </a>
        </li>
        
        <li class="header">Report</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="<?php if($this->uri->segment(2)=="topup"){echo "active";}?>">
          <a href="<?php echo base_url('admin/topup') ?>">
            <i class="fa fa-usd"></i> <span>TopUp</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(2)=="payout"){echo "active";}?>">
          <a href="<?php echo base_url('admin/payout') ?>">
            <i class="fa fa-usd"></i> <span>Payout</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(2)=="dana"){echo "active";}?>">
          <a href="<?php echo base_url('admin/dana') ?>">
            <i class="fa fa-usd"></i> <span>Dana</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(2)=="payment"){echo "active";}?>">
          <a href="<?php echo base_url('admin/payment') ?>">
            <i class="fa fa-money"></i> <span>Payment</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(2)=="invoice"){echo "active";}?>">
          <a href="<?php echo base_url('admin/invoice') ?>">
            <i class="fa fa-money"></i> <span>Invoice</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Tool</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->uri->segment(2)=="user" and $this->uri->segment(3)==" ") {echo "active";}?>">
              <a href="<?php echo base_url('admin/user') ?>">
                <i class="fa fa-users"></i> <span>Users</span>
              </a>
            </li>
            <li class="<?php if($this->uri->segment(3)=="profile"){echo "active";}?>">
              <a href="<?php echo base_url('admin/user/profile') ?>">
                <i class="fa fa-user"></i> <span>Profile</span>
              </a>
            </li>
            <li >
              <a href="<?php echo base_url('admin/awal/logout'); ?>">
                <i class="fa fa-sign-out"></i> <span>Sign Out</span>
              </a>
            </li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">