	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Payment 
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
	          	<div class="box">
		            <div class="box-body table-responsive">
		              <table id="example1" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Kode Invoice</th>
			                  <th>Nama User</th>
			                  <th>Atas Nama</th>
			                  <th>Bank</th>
			                  <th>No. Rekening</th>
			                  <th>Bukti Payment</th>
			                  <th>Status</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php
		                		$start = 0;
		            			foreach ($data_payment as $payment){ 
		                	?>
		                	<tr>
		                		<td><?php echo ++$start ?></td>
		                		<td><?php echo $payment->kode ?></td>
		                		<td><?php echo $payment->nama ?></td>
		                		<td><?php echo $payment->atas_nama ?></td>
		                		<td><?php echo $payment->bank ?></td>
		                		<td><?php echo $payment->no_rek ?></td>
		                		<td>
		                			<img style="width:80px;" src="<?php echo $payment->images; ?>" class="img-thumbnail" alt="User Image" />
		                		</td>
		                		<td><?php echo ucwords($payment->status_payment) ?></td>
		                	</tr>
		                	<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>