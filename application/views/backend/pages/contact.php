	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
    <!-- CKEditor -->
  <script src="<?php echo base_url('assets/backend/bower_components/ckeditor/ckeditor.js'); ?>"></script>
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contact
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form role="form" action="<?php echo base_url('admin/contact/editaction'); ?>" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12">
          <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
          <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="">Alamat Kantor</label>
                    <input type="hidden" class="form-control" id="" name="id" value="<?php echo $id_contact ?>" required>
                    <textarea name="alamat" id="editor1" class="form-control" placeholder="Masukkan alamat kantor" required>
                          <?php echo $alamat ; ?>   
                        </textarea >
                        <script>
                            // Replace the <textarea id="editor1"> with a CKEditor
                            // instance, using default configuration.
                            CKEDITOR.replace( 'editor1' );
                        </script>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="">Contact Person</label>
                    <textarea name="contact_person" id="editor2" class="form-control" placeholder="Masukkan Contact Person" required>
                          <?php echo $contact_person ; ?>   
                        </textarea >
                        <script>
                            // Replace the <textarea id="editor1"> with a CKEditor
                            // instance, using default configuration.
                            CKEDITOR.replace( 'editor2' );
                        </script>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="">Legal Information</label>
                    <textarea name="legal_information" id="editor3" class="form-control" placeholder="Masukkan Legal Information" required>
                          <?php echo $legal_information ; ?>   
                        </textarea >
                        <script>
                            // Replace the <textarea id="editor1"> with a CKEditor
                            // instance, using default configuration.
                            CKEDITOR.replace( 'editor3' );
                        </script>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pastikan semua data telah terisi</h3> 
            </div>
            <div class="box-body">
              <button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-save"></i> Simpan Data</button>
              <a href="<?php echo base_url('admin') ?>"><span class="btn btn-flat btn-danger"><i class="fa fa-ban"></i> Batal</span></a>
            </div>
          </div>
        </div>
        <!-- /.col -->
        </form>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	  <?php $this->load->view('backend/js'); ?>
    <!-- Select2 -->
    <script src="<?php echo base_url('assets/backend/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
    <script>
      $(function () {
        $('.select2').select2();
      })
    </script>

	</body>
</html>