  <!-- Header -->
  <?php $this->load->view('backend/header'); ?>
  <!-- Css -->
  <?php $this->load->view('backend/css'); ?>
    <!-- Menu -->
  <?php $this->load->view('backend/menu'); ?>

  <?php
    if ($category=="driver") {
      $potongan=($cost*5/100)+5000;
      $origins=urlencode($origin);
      $destinations=urlencode($destination);
      $data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$origins&destinations=$destinations&language=id-ID&sensor=false");
      $data = json_decode($data);
      $distance = 0;

      foreach($data->rows[0]->elements as $road) {
          $distance += $road->distance->value;
      }
    }else{
      $potongan=($cost*10/100)+5000;
      $data_wisata= explode(" ", $wisata);
      $jml_penumpang=$data_wisata[0]+$data_wisata[1];
    }

  ?>
   
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice <small><?php echo $kode; ?></small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Invoice Kodaja Trans.
            <small class="pull-right">Date: <?php echo $create_on; ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Customers
          <address>
            <strong><?php echo $nama_customer; ?></strong><br>
            <?php echo $alamat_customer; ?><br>
            Phone : <?php echo $hp_customer; ?><br>
            Email : <?php echo $email_customer; ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Driver
          <address>
            <strong><?php echo $nama_driver; ?></strong><br>
            <?php echo $alamat_driver; ?><br>
            Phone : <?php echo $hp_driver; ?><br>
            Email : <?php echo $email_driver; ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice <?php echo $kode; ?></b><br>
          <br>
          <b>Origin : </b> <?php echo $origin; ?><br>
          <b>Destination : </b> <?php echo $destination; ?><br>
          <b>Status Invoice : </b> <?php echo $status; ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr style="border: 1px solid #ddd">
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <?php if($category=="wisata"){ ?>
              <thead>
                <tr>
                  <th>Nama Wisata</th>
                  <th>Harga</th>
                  <th>Jumlah Penumpang</th>
                  <th class="text-center">Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo $nama_wisata; ?></td>
                  <td>Rp. <?php echo number_format($harga); ?></td>
                  <td><?php echo $jml_penumpang; ?></td>
                  <td align="right">Rp. <?php echo number_format($cost); ?></td>
                </tr>
              </tbody>
            <?php }else{ ?>
              <thead>
                <tr>
                  <th width="350">Origin</th>
                  <th width="350">Destination</th>
                  <th>Jumlah KM</th>
                  <th class="text-center">Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo $origin; ?></td>
                  <td><?php echo $destination; ?></td>
                  <td><?php echo floor($distance/1000) ?> Km</td>
                  <td align="right">Rp. <?php echo number_format($cost); ?></td>
                </tr>
              </tbody>
            <?php } ?>
              <tfoot>
                <tr style="padding-bottom: 20px;margin-top: 20px;border:none;border-bottom: 2px solid #ddd">
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" style="border:none;"></td>
                  <td><b>Total Harga : </b></td>
                  <td align="right">Rp. <?php echo number_format($cost); ?></td>
                </tr>
                <tr>
                  <td colspan="2" style="border:none;"></td>
                  <td><b>Kodaja <?php if($category=="wisata"){echo "(5%)";}else{echo "(10%)";} ?> : </b></td>
                  <td align="right">Rp. <?php echo number_format($potongan-5000); ?></td>
                </tr>
                <tr>
                  <td colspan="2" style="border:none;"></td>
                  <td><b>Desa Adat : </b></td>
                  <td align="right">Rp. <?php echo number_format(5000); ?></td>
                </tr>
                <tr>
                  <td colspan="2" style="border:none;"></td>
                  <td><b>Yang Diterima Driver : </b></th>
                  <td align="right">Rp. <?php echo number_format($cost-$potongan); ?></td>
                </tr>
              </tfoot>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="<?php echo base_url('admin/invoice/cetak/'.$kode) ?>" target="_blank" class="btn btn-default pull-right"><i class="fa fa-print"></i> Print</a>
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
  <?php $this->load->view('backend/js'); ?>
  </body>
</html>
