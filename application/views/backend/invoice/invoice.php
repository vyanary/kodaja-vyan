	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Invoice 
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
	        	<span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
          		<span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
	          	<div class="box">
		            <div class="box-body table-responsive">
		              <table id="example1" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Kode Invoice</th>
			                  <th>Nama Customers</th>
			                  <th>Nama Driver</th>
			                  <th>Origin</th>
			                  <th>Destination</th>
			                  <th>Cost</th>
			                  <th>Wisata</th>
			                  <th>Status</th>
			                  <th>Tanggal</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php
		                		$start = 0;
		            			foreach ($data_invoice as $invoice){
		            				$id_driver = array('id_user' => $invoice->driver_id, );
		            				$data_driver=$this->db->get_where('tb_user',$id_driver)->row();
		                	?>
		                	<tr>
		                		<td><?php echo ++$start ?></td>
		                		<td><a href="<?php echo base_url('admin/invoice/detail/'.$invoice->kode) ?>" class="label label-primary"><?php echo $invoice->kode ?></a></td>
		                		<td><?php echo $invoice->nama ?></td>
		                		<td><?php echo $data_driver->nama ?></td>
		                		<td><?php echo $invoice->origin ?></td>
		                		<td><?php echo $invoice->destination ?></td>
		                		<td>Rp. <?php echo number_format($invoice->cost) ?></td>
		                		<td><?php echo $invoice->nama_wisata ?></td>
		                		<td><?php echo ucwords($invoice->status) ?></td>
		                		<td><?php echo ucwords($invoice->create_on) ?></td>
		                	</tr>
		                	<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>