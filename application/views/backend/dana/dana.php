	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Dana 
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
	          	<div class="box">
		            <div class="box-body table-responsive">
		              <table id="example1" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Kode Invoice</th>
			                  <th>Cost</th>
			                  <th>Kodaja</th>
			                  <th>Adat</th>
			                  <th>Driver</th>
			                  <th>Status Customers</th>
			                  <th>Status Driver</th>
			                  <th>Tanggal</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php
		                		$start = 0;
		            			foreach ($data_dana as $dana){ 
		                	?>
		                	<tr>
		                		<td><?php echo ++$start ?></td>
		                		<td><?php echo $dana->invoice_id ?></td>
		                		<td><?php echo $dana->cost ?></td>
		                		<td><?php echo $dana->kodaja ?></td>
		                		<td><?php echo $dana->adat ?></td>
		                		<td><?php echo $dana->driver ?></td>
		                		<td><?php echo $dana->status_customer ?></td>
		                		<td><?php echo $dana->status_driver ?></td>
		                		<td><?php echo $dana->create_on ?></td>
		                	</tr>
		                	<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>