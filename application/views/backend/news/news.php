	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data News
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
		        <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
		        <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
	          	<div class="box">
		            <div class="box-header">
		              <a href="<?php echo base_url('admin/news/tambah') ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah News</a>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body table-responsive">
		              <table id="example1" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  <th>No.</th>
			                  <th>Judul</th>
			                  <th>Isi</th>
			                  <th>Image</th>
			                  <th>Tanggal Post</th>
			                  <th>Aksi</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php
		                		$start = 0;
		            			foreach ($data_news as $news){ 
		                	?>
		                	<tr>
		                		<td><?php echo ++$start ?></td>
		                		<td><?php echo $news->judul ?></td>
		                		<td>
		                			<?php echo substr($news->isi, 0,70);  ?><br>...
		                			<a data-toggle="modal" title="Read More" data-target="#modalisi<?php echo $start ?>" >Read More</a>
			                  		<div class="modal fade" id="modalisi<?php echo $start ?>">
  							          	<div class="modal-dialog">
  							            	<div class="modal-content">
  							              		<div class="modal-header">
  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							                  		<span aria-hidden="true">&times;</span></button>
  							                		<h4 class="modal-title">Isi</h4>
  							              		</div>
  							              		<div class="modal-body">
  							                		<?php echo $news->isi; ?>
  							              		</div>
  							            	</div>
  							          	</div>
  							        </div>
		                		</td>
		                		<td>
		                			<img style="width:80px;" src="<?php echo $news->image; ?>" class="img-thumbnail" alt="User Image" />
			                    </td>
			                    <td><?php echo $news->tanggal ?></td>
			                    <td align="center">
			                    	<a title="Edit" class="btn btn-warning btn-sm" href="<?php echo base_url('admin/news/edit/'.$news->id_news); ?>"><i class="fa fa-pencil"></i></a>
			                    	<a title="Hapus" data-toggle="modal" data-target="#modalhapus<?php echo $start ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
			                    	<!-- Modal Hapus -->
			                    	<div class="modal fade" id="modalhapus<?php echo $start ?>">
							          	<div class="modal-dialog modal-sm">
							            	<div class="modal-content">
							              		<div class="modal-header">
							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                  		<span aria-hidden="true">&times;</span></button>
							                		<h4 class="modal-title">Peringatan</h4>
							              		</div>
							              		<div class="modal-body">
							                		<p>Yakin Ingin Menghapus data ?</p>
							              		</div>
							              		<div class="modal-footer">
							              			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
							                		<a href="<?php echo base_url('admin/news/hapus/'.$news->id_news); ?>" class="btn btn-primary" style="margin-right: 10px;">Yes</a>
							              		</div>
							            	</div>
							          	</div>
							        </div>
			                    </td>
		                	</tr>
		                	<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>