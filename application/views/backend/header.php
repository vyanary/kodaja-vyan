<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Favicons -->
  <link rel="icon" href="<?php echo base_url(); ?>/assets/frontend/img/favicon.png">
  <title><?php if($page_title!=" "){echo $page_title." | ";} ?>Admin Kodaja </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">