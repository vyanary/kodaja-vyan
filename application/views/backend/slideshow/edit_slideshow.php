  <!-- Header -->
  <?php $this->load->view('backend/header'); ?>
  <!-- Css -->
  <?php $this->load->view('backend/css'); ?>
  <!-- CKEditor -->
  <script src="<?php echo base_url('assets/backend/bower_components/ckeditor/ckeditor.js'); ?>"></script>
    <!-- Menu -->
  <?php $this->load->view('backend/menu'); ?>
   
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Slideshow
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form role="form" action="<?php echo base_url('admin/slideshow/editaction'); ?>" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-6" > 
                      <div class="form-group">
                          <label for="">Nama Slide</label>
                          <input type="hidden" class="form-control" value="<?=$id_slide?>" id="" name="id" placeholder="Masukkan Judul slideshow" required>
                          <input type="text" class="form-control" value="<?=$nama_slide?>" id="" name="nama_slide" placeholder="Masukkan Judul slideshow" required>
                      </div>
                      <div class="form-group">
                          <label for="">Text Slide </label>
                          <textarea name="text" id="editor1" class="form-control" placeholder="Masukkan Text slideshow"><?=$text?></textarea>
                      </div>
                    </div>
                    <div class="col-lg-6" >
                      <div class="form-group">
                          <label for="">Kategori Slide </label>
                          <select class="form-control select2" required name="kategori" style="width: 100%;">
                            <option value="">--Pilih Kategori--</option>
                            <option value="android" <?php if($kategori=="android"){echo "SELECTED";} ?>>Android</option>
                            <option value="website" <?php if($kategori=="website"){echo "SELECTED";} ?>>Website</option>
                          </select>
                      </div>
                      <div class="form-group">
                        <label>Image</label>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <div class="col-md-12" style="border:1px solid #ddd;padding:5px;margin-top: -15px;margin-left:-15px;">
                              <img src="<?=$image?>" class="img-responsive" id="preview_foto">
                            </div>
                            <div class="clearfix"></div>
                            <p class="small" style="margin-left:-15px;margin-bottom: 10px;">*Max file size 2MB </p>
                            <div class="col-sm-offset-2" style="margin-left:25%;">
                              <div class="button-upload btn btn-flat btn-primary width-183px">
                                  <span><i class="fa fa-folder-open"></i> Pilih Foto</span>
                                  <input name="nama_file" class="upload" type="file" accept="image/*" onchange="tampilkanPreview(this,'preview_foto')"/> 
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pastikan semua data telah terisi</h3> 
            </div>
            <div class="box-body">
              <button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-save"></i> Simpan Data</button>
              <a href="<?php echo base_url('admin/slideshow') ?>"><span class="btn btn-flat btn-danger"><i class="fa fa-ban"></i> Batal</span></a>
            </div>
          </div>
        </div>
        <!-- /.col -->
        </form>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
    <?php $this->load->view('backend/js'); ?>
    <!-- Select2 -->
    <script src="<?php echo base_url('assets/backend/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
    <!-- Preview foto -->
    <script>
      $(function () {
      $('.select2').select2();
    })
      
      function tampilkanPreview(gambar,idpreview){
          var gb = gambar.files;
          
          for (var i = 0; i < gb.length; i++){
              var gbPreview = gb[i];
              var imageType = /image.*/;
              var preview=document.getElementById(idpreview);            
              var reader = new FileReader();
              
              if (gbPreview.type.match(imageType)) {
                  preview.file = gbPreview;
                  reader.onload = (function(element) { 
                      return function(e) { 
                          element.src = e.target.result; 
                      }; 
                  })(preview);

                  reader.readAsDataURL(gbPreview);
              }else{
                  alert("Type file tidak sesuai. Khusus image.");
              }
          }    
      }
    </script>
     <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>
  </body>
</html>