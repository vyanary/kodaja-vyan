	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
  <!-- CKEditor -->
  <script src="<?php echo base_url('assets/backend/bower_components/ckeditor/ckeditor.js'); ?>"></script>
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah wisata
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form role="form" action="<?php echo base_url('admin/wisata/simpan'); ?>" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-12">
                	<div class="row">
                		<div class="col-lg-12" > 
                			<div class="form-group">
			                    <label for="">Nama Wisata</label>
			                    <input type="text" class="form-control" value="" id="" name="nama_wisata" placeholder="Masukkan Nama wisata" required>
			                </div>
                    </div>
                    <div class="col-lg-6">
                			<div class="form-group">
			                    <label for="">Keterangan </label>
                          <textarea required name="keterangan" id="editor1" class="form-control" placeholder="Masukkan Keterangan wisata"></textarea >
			                </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label for="">Fasilitas </label>
                          <textarea required name="fasilitas" id="editor2" class="form-control" placeholder="Masukkan Fasilitas wisata"></textarea >
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label for="">Harga</label>
                          <input type="number" class="form-control" value="" id="" name="harga" placeholder="Masukkan harga" required style="text-align: right;" >
                      </div>
                    </div>
                    <div class="col-lg-6">
                			<div class="form-group">
                        <label>Image</label>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <div class="col-md-12" style="border:1px solid #ddd;padding:5px;margin-top: -15px;margin-left:-15px;" align="center">
                              <img src="<?php echo base_url('assets/backend/img/nopicture.jpg') ?>" class="img-responsive" id="preview_foto">
                            </div>
                            <div class="clearfix"></div>
                            <p class="small" style="margin-left:-15px;margin-bottom: 10px;">*Max file size 2MB </p>
                            <div class="col-sm-offset-2" style="margin-left:25%;">
                              <div class="button-upload btn btn-flat btn-primary width-183px">
                                  <span><i class="fa fa-folder-open"></i> Pilih Foto</span>
                                  <input name="nama_file" class="upload" type="file" accept="image/*" required onchange="tampilkanPreview(this,'preview_foto')"/> 
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                		</div>
                	</div>
                </div>
              </div><!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pastikan semua data telah terisi</h3> 
            </div>
            <div class="box-body">
              <button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-plus"></i> Tambah Data</button>
              <a href="<?php echo base_url('admin/wisata') ?>"><span class="btn btn-flat btn-danger"><i class="fa fa-ban"></i> Batal</span></a>
            </div>
          </div>
        </div>
        <!-- /.col -->
        </form>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	  <?php $this->load->view('backend/js'); ?>
    <!-- Preview Image -->
    <script type="text/javascript">
      
      function tampilkanPreview(gambar,idpreview){
          var gb = gambar.files;
          
          for (var i = 0; i < gb.length; i++){
              var gbPreview = gb[i];
              var imageType = /image.*/;
              var preview=document.getElementById(idpreview);            
              var reader = new FileReader();
              
              if (gbPreview.type.match(imageType)) {
                  preview.file = gbPreview;
                  reader.onload = (function(element) { 
                      return function(e) { 
                          element.src = e.target.result; 
                      }; 
                  })(preview);

                  reader.readAsDataURL(gbPreview);
              }else{
                  alert("Type file tidak sesuai. Khusus image.");
              }
          }    
      }
    </script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
        CKEDITOR.replace( 'editor2' );
    </script>
	</body>
</html>