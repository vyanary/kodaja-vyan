	<!-- Header -->
	<?php $this->load->view('backend/header'); ?>
	<!-- Css -->
	<?php $this->load->view('backend/css'); ?>
	<!-- DataTables -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Menu -->
	<?php $this->load->view('backend/menu'); ?>
	 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Wisata
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12">
		        <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
		        <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
	          	<div class="box">
		            <div class="box-header">
		              <a href="<?php echo base_url('admin/wisata/tambah') ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah wisata</a>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body table-responsive">
		              <table id="example1" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  	<th>No.</th>
			                  	<th width="100px">Nama Wisata</th>
			                  	<th width="200px">Keterangan</th>
                        		<th width="200px">Fasilitas</th>
			                  	<th>Harga</th>
			                  	<th>Image</th>
			                  	<th>Tanggal</th>
			                  	<th>Aksi</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php
		                		$start = 0;
		            			foreach ($data_wisata as $wisata){ 
		                	?>
		                	<tr>
		                		<td><?php echo ++$start ?></td>
		                		<td><?php echo $wisata->nama_wisata ?></td>
			                  	<td>
			                  		<?php echo substr($wisata->keterangan, 0,70) ?><br>...
			                  		<a title="Read More" data-toggle="modal" data-target="#modalketerangan<?php echo $start ?>" >Read More</a>
			                  		<div class="modal fade" id="modalketerangan<?php echo $start ?>">
  							          	<div class="modal-dialog">
  							            	<div class="modal-content">
  							              		<div class="modal-header">
  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							                  		<span aria-hidden="true">&times;</span></button>
  							                		<h4 class="modal-title">Keterangan</h4>
  							              		</div>
  							              		<div class="modal-body">
  							                		<?php echo $wisata->keterangan; ?>
  							              		</div>
  							            	</div>
  							          	</div>
  							        </div>
			                  	</td>
			                  	<td>
			                  		<?php echo substr($wisata->fasilitas, 0,70) ?><br>...
			                  		<a data-toggle="modal" title="Read More" data-target="#modalfasilitas<?php echo $start ?>" >Read More</a>
			                  		<div class="modal fade" id="modalfasilitas<?php echo $start ?>">
  							          	<div class="modal-dialog">
  							            	<div class="modal-content">
  							              		<div class="modal-header">
  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							                  		<span aria-hidden="true">&times;</span></button>
  							                		<h4 class="modal-title">Fasilitas</h4>
  							              		</div>
  							              		<div class="modal-body">
  							                		<?php echo $wisata->fasilitas; ?>
  							              		</div>
  							            	</div>
  							          	</div>
  							        </div>
			                  	</td>
			                  	<td><?php echo $wisata->harga ?></td>
			                  	<td>
		                			<img style="width:80px;" src="<?php echo $wisata->image; ?>" class="img-thumbnail" alt="User Image" />
			                  	</td>
			                  	<td><?php echo $wisata->date ?></td>
			                  	<td align="center">
		                    		<a class="btn btn-warning btn-sm" title="Edit" href="<?php echo base_url('admin/wisata/edit/'.$wisata->id_wisata); ?>"><i class="fa fa-pencil"></i></a>
		                    		<a data-toggle="modal" title="Hapus" data-target="#modalhapus<?php echo $start ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
			                    	<!-- Modal Hapus -->
				                    <div class="modal fade" id="modalhapus<?php echo $start ?>">
  							          	<div class="modal-dialog modal-sm">
  							            	<div class="modal-content">
  							              		<div class="modal-header">
  							                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							                  		<span aria-hidden="true">&times;</span></button>
  							                		<h4 class="modal-title">Peringatan</h4>
  							              		</div>
  							              		<div class="modal-body">
  							                		<p>Yakin Ingin Menghapus data ?</p>
  							              		</div>
  							              		<div class="modal-footer">
  							              			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
  							                		<a href="<?php echo base_url('admin/wisata/hapus/'.$wisata->id_wisata); ?>" class="btn btn-primary" style="margin-right: 10px;">Yes</a>
  							              		</div>
  							            	</div>
  							          	</div>
  							        </div>
			                  	</td>
		                	</tr>
		                	<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- Footer -->
    <?php $this->load->view('backend/footer'); ?>
    <!-- JS -->
	<?php $this->load->view('backend/js'); ?>
	<!-- DataTables -->
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#example1').DataTable()
	  })
	</script>
	</body>
</html>