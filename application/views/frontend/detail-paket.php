<?php $this->load->view('frontend/header'); ?>
<?php $this->load->view('frontend/css'); ?> 
<?php $this->load->view('frontend/menu'); ?> 
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;<?php echo $image ?>&apos;);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h1 class="title"><?php echo $nama_wisata; ?></h1>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="section" id="carousel">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <!-- Carousel Card -->
                    <div class="card card-raised card-carousel">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="<?php echo $image ?>" alt="First slide">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Carousel Card -->
                </div>
                <div class="col-md-5">
                    <div class="info info-horizontal mt-3 py-0">
                        <h2>Rp. <?php echo str_replace(",", ".", number_format($harga)); ?> / orang</h2>
                        <p>*Harga diatas dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu</p>
                        <form method="post" action="#">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Perjalanan</label>
                                        <input type="text" class="form-control datetimepicker" value="03/02/2018 4:58 PM" style="height: 42px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Orang</label>
                                        <select class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-success" type="submit"><i class="fa fa-send"></i> Pesan Sekarang</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section pt-0" id="tabspaket">
        <div class="container">
            <div class="card card-nav-tabs mt-0">
                <div class="card-header card-header-rose">
                    <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#keterangan" data-toggle="tab">
                                        <i class="material-icons">description</i> Keterangan
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#fasilitas" data-toggle="tab">
                                        <i class="material-icons">assessment</i> Fasilitas
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#paketlain" data-toggle="tab">
                                        <i class="material-icons">next_week</i> Paket Lainnya
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="tab-content text-center">
                        <div class="tab-pane active text-justify" id="keterangan">
                            <?php echo $keterangan; ?>
                        </div>
                        <div class="tab-pane text-justify" id="fasilitas">
                            <?php echo $fasilitas; ?>
                        </div>
                        <div class="tab-pane" id="paketlain">
                            <div class="section text-center">
                                <div class="row mt-3">
                                    <?php
                                        $start=0;
                                        foreach ($data_wisata as $wisata){ 
                                        ++$start;
                                    ?>
                                    <div class="col-md-4">
                                        <div class="card card-blog">
                                            <div class="card-header card-header-image">
                                                <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>">
                                                    <img src="<?php echo $wisata->image ?>" alt="">
                                                </a>
                                            </div>
                                            <div class="card-body text-justify">
                                                <h4 class="card-title text-center">
                                                    <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>"><?php echo $wisata->nama_wisata; ?></a>
                                                </h4>
                                                <p class="card-description">
                                                    <?php echo substr($wisata->keterangan, 0,200); ?>...
                                                </p>
                                                <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>" class="btn btn-success">
                                                    <i class="material-icons">visibility</i> Detail
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/footer'); ?>
<?php $this->load->view('frontend/js'); ?>
</body>
</html>