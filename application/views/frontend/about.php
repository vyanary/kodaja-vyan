<?php $this->load->view('frontend/header'); ?>
<?php $this->load->view('frontend/css'); ?> 
<?php $this->load->view('frontend/menu'); ?> 
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;<?php echo $image?>&apos;);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h1 class="title"><?php echo $judul ?></h1>
                <h4><?php echo $sub_judul ?></h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <div class="section text-center">
           <p class="text-justify">
                <?php echo $isi ?>
           </p>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/footer'); ?>
<?php $this->load->view('frontend/js'); ?>

</body>
</html>
