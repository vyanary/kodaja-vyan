<?php

    $about = $this->M_pages->get_by_kat_and_id('website',2);
    $contact = $this->M_contact->get_all();
?>
<footer class="footer bg-dark mt-5 footer-big">
    <div class="container">
        <div class="content text-left mb-3 pb-3" style="border-bottom: 1px solid #6D6F72">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="text-white">About Us</h5>
                    <p><?php echo substr($about->isi, 0,500); ?>...<a href="<?php echo base_url('about') ?>">Read More</a></p>
                </div>
                <div class="col-md-4">
                    <h5 class="text-white">Social Feed</h5>
                    <ul class="float-left text-white">
                        <li>
                            <a style="font-size: 32px" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" data-original-title="Follow us on Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a style="font-size: 32px" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" data-original-title="Like us on Facebook">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </li>
                        <li>
                            <a style="font-size: 32px" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" data-original-title="Follow us on Instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="info info-horizontal my-0 py-0">
                        <div class="icon icon-success">
                            <i class="material-icons">pin_drop</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title text-white">Find us at the office</h4>
                            <?php echo $contact->alamat; ?>
                        </div>
                    </div>
                    <div class="info info-horizontal my-0 py-0">
                        <div class="icon icon-success">
                            <i class="material-icons">phone</i>
                        </div>
                        <div class="description ">
                            <h4 class="info-title text-white">Give us a ring</h4>
                            <?php echo $contact->contact_person; ?>
                        </div>
                    </div>
                    <div class="info info-horizontal my-0 py-0">
                        <div class="icon icon-success">
                            <i class="material-icons">business_center</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title text-white">Legal Information</h4>
                            <?php echo $contact->legal_information; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ul class="float-left text-white">
            <li>
                <a href="#">
                    Disclaimer
                </a>
            </li>
            <li>
                <a href="#">
                    Privacy Police
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('about'); ?>">
                    About Us
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('paket'); ?>">
                    Paket Wisata
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('contact'); ?>">
                    Contact Us
                </a>
            </li>
        </ul>
        <div class="copyright float-right text-white">
            Copyright &#xA9;
            <script>
                document.write(new Date().getFullYear())
            </script> Your Company All Rights Reserved.
        </div>
    </div>
</footer>