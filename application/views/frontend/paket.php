<?php $this->load->view('frontend/header'); ?>
<?php $this->load->view('frontend/css'); ?> 
<?php $this->load->view('frontend/menu'); ?> 
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;<?php echo $image?>&apos;);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h1 class="title"><?php echo $judul ?></h1>
                <h4><?php echo $sub_judul ?></h4>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <div class="section text-center">
            <div class="row mt-3">
                <?php
                    $start=0;
                    foreach ($data_wisata as $wisata){ 
                    ++$start;
                ?>
                <div class="col-md-4">
                    <div class="card card-blog">
                        <div class="card-header card-header-image">
                            <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>">
                                <img src="<?php echo $wisata->image ?>" alt="">
                            </a>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>"><?php echo $wisata->nama_wisata; ?></a>
                            </h4>
                            <p class="card-description">
                                <?php echo substr($wisata->keterangan, 0,200); ?>...
                            </p>
                            <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>" class="btn btn-success">
                                <i class="material-icons">visibility</i> Detail
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/footer'); ?>
<?php $this->load->view('frontend/js'); ?>
</body>
</html>
