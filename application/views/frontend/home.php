<?php $this->load->view('frontend/header'); ?>
<?php $this->load->view('frontend/css'); ?> 
<?php $this->load->view('frontend/menu'); ?> 
<div class="card card-raised card-carousel mt-0 pt-0">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators mb-5">
            <?php
                for ($i=0; $i < $total_slideshow; $i++) { 
            ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i ?>" <?php if($i==0){echo 'class="active"';} ?>></li>
            <?php } ?>
        </ol>
        <div class="carousel-inner">
            <?php
                $start=0;
                foreach ($data_slideshow as $slideshow){ 
                ++$start;
            ?>
            <div class="carousel-item <?php if($start==1){echo 'active';} ?>">
                <img class="d-block w-100" src="<?php echo $slideshow->image ?>" alt="slide<?php echo $start ?>">
                <div style=" background-color:rgba(0,0,0,0.6);position: absolute;top:0px;min-height: 2000px;z-index: 10;" class="w-100 d-block">
                    &nbsp;
                </div>
                <div class="carousel-caption d-none d-md-block mb-3">
                    <h4>
                        <?php echo $slideshow->text ?>
                    </h4>
                </div>
            </div>
            <?php } ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <i class="material-icons">keyboard_arrow_left</i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <i class="material-icons">keyboard_arrow_right</i>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<!-- End Carousel Card -->
<div class="main main-raised">
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h2 class="title"><?php echo $judul; ?></h2>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-4">
                    <img src="<?php echo $image; ?>" class="img-fluid">
                </div>
                <div class="col-md-8">
                    <p class="text-justify">
                        <?php echo $isi; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="section text-center">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="title">Paket Wisata</h2>
                    <h5 class="description">Berbagai destinasi wisata yang telah kami siapkan untuk Anda dan keluarga. Are you ready for adventure?.</h5>
                </div>
            </div>
            <div class="row mt-3">
                <?php
                    $start=0;
                    foreach ($data_wisata as $wisata){ 
                    ++$start;
                ?>
                <div class="col-md-4">
                    <div class="card card-blog">
                        <div class="card-header card-header-image">
                            <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>">
                                <img src="<?php echo $wisata->image ?>" alt="">
                            </a>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>"><?php echo $wisata->nama_wisata; ?></a>
                            </h4>
                            <p class="card-description">
                                <?php echo substr($wisata->keterangan, 0,200); ?>...
                            </p>
                            <a href="<?php echo base_url('/paket/detail/'.$wisata->id_wisata) ?>" class="btn btn-success">
                                <i class="material-icons">visibility</i> Detail
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/footer'); ?>
<?php $this->load->view('frontend/js'); ?>

</body>
</html>