    <!--   Core JS Files   -->
    <script src="<?php echo base_url(); ?>assets/frontend/js/core/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/core/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-material-design.js"></script>
    <!--  Plugin for Date Time Picker and Full Calendar Plugin  -->
    <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/moment.min.js"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/nouislider.min.js"></script>
    <!-- Material Kit Core initialisations of plugins and Bootstrap Material Design Library -->
    <script src="<?php echo base_url(); ?>assets/frontend/js/material-kit.js?v=2.0.2"></script>
    <!-- DESIGN BY : VYAN ARY PRATAMA -->
