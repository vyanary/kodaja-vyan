</head>
<body>
    <nav class="navbar navbar-color-on-scroll navbar-dark bg-rose fixed-top navbar-expand-lg " color-on-scroll="100" id="sectionsNav">
        <div class="container">
            <div class="navbar-translate">

                <a class="navbar-brand" href="<?php echo base_url(); ?>">
                    <img src="<?php echo base_url(); ?>assets/frontend/img/LOGO.png" height="30" alt=""><!-- Kodaja Trans Bali  -->
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item <?php if($this->uri->segment(1)==""){echo "active";}?>">
                        <a class="nav-link" href="<?php echo base_url(); ?>" >
                            <i class="material-icons">home</i> Home
                        </a>
                    </li>
                    <li class="nav-item <?php if($this->uri->segment(1)=="paket"){echo "active";}?>">
                        <a class="nav-link" href="<?php echo base_url('paket'); ?>" >
                            <i class="material-icons">card_travel</i> Paket Wisata
                        </a>
                    </li>
                    <li class="nav-item <?php if($this->uri->segment(1)=="about"){echo "active";}?>">
                        <a class="nav-link" href="<?php echo base_url('about'); ?>" >
                            <i class="material-icons">people</i> About Us
                        </a>
                    </li>
                    <li class="nav-item <?php if($this->uri->segment(1)=="contact"){echo "active";}?>">
                        <a class="nav-link" href="<?php echo base_url('contact'); ?>" >
                            <i class="material-icons">contacts</i> Contact Us
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" data-original-title="Follow us on Twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" data-original-title="Like us on Facebook">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" data-original-title="Follow us on Instagram">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>