<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Favicons -->
      <link rel="icon" href="<?php echo base_url(); ?>/assets/frontend/img/favicon.png">
    <title>
        <?php if($page_title!=" "){echo $page_title." | ";} ?>Kodaja Trans Bali
    </title>