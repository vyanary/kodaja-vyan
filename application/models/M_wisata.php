<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_wisata extends CI_Model
{

    public $table = 'tb_wisata';
    public $id = 'id_wisata';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_where($where)
    {
        $this->db->where($where);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_wisata', $q);
    	$this->db->or_like('nama_wisata', $q);
    	$this->db->or_like('keterangan', $q);
        $this->db->or_like('fasilitas', $q);
        $this->db->or_like('harga', $q);
        $this->db->or_like('image', $q);
        $this->db->or_like('date', $q);
    	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->like('id_wisata', $q);
        $this->db->or_like('nama_wisata', $q);
        $this->db->or_like('keterangan', $q);
        $this->db->or_like('fasilitas', $q);
        $this->db->or_like('harga', $q);
        $this->db->or_like('image', $q);
        $this->db->or_like('date', $q);
	    $this->db->limit($limit, $start);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}