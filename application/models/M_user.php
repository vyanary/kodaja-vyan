<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_user extends CI_Model
{

    public $table = 'tb_user';
    public $id = 'id_user';
    public $order = 'DESC';
    public $admin='admin';
    public $kat = 'kategori';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_where($where)
    {
        $this->db->where($where);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get data by kategori
    function get_by_kat($kat)
    {
        $this->db->where($this->kat, $kat);
        return $this->db->get($this->table)->result();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_user', $q);
    	$this->db->or_like('nama', $q);
    	$this->db->or_like('alamat', $q);
        $this->db->or_like('tgl_lahir', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('no_hp', $q);
        $this->db->or_like('status', $q);
        $this->db->or_like('sim', $q);
        $this->db->or_like('kategori', $q);
        $this->db->or_like('foto_user', $q);
        $this->db->or_like('lat', $q);
        $this->db->or_like('ket', $q);
        $this->db->or_like('date', $q);
    	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->like('id_user', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('alamat', $q);
        $this->db->or_like('tgl_lahir', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('no_hp', $q);
        $this->db->or_like('status', $q);
        $this->db->or_like('sim', $q);
        $this->db->or_like('kategori', $q);
        $this->db->or_like('foto_user', $q);
        $this->db->or_like('lat', $q);
        $this->db->or_like('ket', $q);
        $this->db->or_like('date', $q);
	    $this->db->limit($limit, $start);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_limit_data_row($limit, $start = 0) {
        $this->db->limit($limit, $start);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function cek_login($where){      
        return $this->db->get_where($this->table,$where);
    }

    function cek_user($where){ 
        $this->db->where($where);  
        $this->db->where('kategori !=', $this->admin);   
        return $this->db->get($this->table);
    }

}