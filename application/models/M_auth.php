<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class M_auth extends CI_Model{      

    public $client_service='kodaja-client';
    public $auth_key='kodajatransbali';

    function check_auth_client(){
    	$input_client_service = $this->input->get_request_header('Client_Service', TRUE);
    	$input_auth_key = $this->input->get_request_header('Auth_Key', TRUE);
    	if ($input_client_service == $this->client_service && $input_auth_key == $this->auth_key) {
    		return TRUE;
    	}else{
    		return FALSE;
    	}
    }

    function auth(){
        $user_id=$this->input->get_request_header('User-ID', TRUE);
        $token=$this->input->get_request_header('Authorization', TRUE);
        if ($user_id=="") {
            return array('status' => 401,'pesan' => "Unauthorized Header", );
        }elseif ($token=="") {
            return array('status' => 401,'pesan' => "Unauthorized Header", );
        }else{
            $cek_id=$this->db->where('id_user', $user_id)->get('tb_user')->num_rows();
            $data_user_where_id=$this->db->where('id_user', $user_id)->get('tb_user')->row();
            if ($cek_id > 0) {
                $where = array('id_user' => $data_user_where_id->id_user,
                               'token' => $token, );
                $cek_token=$this->db->get_where('tb_user', $where)->num_rows();
                if ($cek_token > 0) {
                    return array('status' => 200,'pesan' => "Aauthorized", );
                }else{
                    $data_token = array('token' => $token,);
                    $this->db->where('id_user', $user_id)->update('tb_user', $data_token);
                    return array('status' => 200,'pesan' => "Aauthorized", );
                }
            }else{
                return array('status' => 401,'pesan' => "Unauthorized Header", );
            }
            
        }
    }
}