<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_invoice extends CI_Model
{

    public $table = 'tb_invoice';
    public $id = 'kode';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->select("tb_invoice.kode, tb_invoice.wisata_id, tb_invoice.driver_id, tb_wisata.nama_wisata, tb_wisata.harga, tb_invoice.wisata, tb_user.nama, tb_invoice.cost, tb_invoice.origin, tb_invoice.destination, tb_invoice.status, tb_invoice.category,  tb_invoice.create_on,");
        $this->db->join('tb_user', 'tb_user.id_user = tb_invoice.user_id', 'left');
        $this->db->join('tb_wisata', 'tb_wisata.id_wisata = tb_invoice.wisata_id', 'left');
        $this->db->order_by('tb_invoice.create_on', $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_by_id($kode)
    {
        $this->db->select("tb_invoice.kode, tb_invoice.wisata_id, tb_invoice.wisata, tb_invoice.driver_id, tb_wisata.nama_wisata, tb_invoice.wisata, tb_wisata.harga, tb_user.nama, tb_user.no_hp, tb_user.email, tb_user.alamat, tb_invoice.cost, tb_invoice.origin, tb_invoice.destination, tb_invoice.status, tb_invoice.category, tb_invoice.create_on,");
        $this->db->join('tb_user', 'tb_user.id_user = tb_invoice.user_id', 'left');
        $this->db->join('tb_wisata', 'tb_wisata.id_wisata = tb_invoice.wisata_id', 'left');
        $this->db->order_by($this->id, $this->order);
        $this->db->where($this->id, $kode);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }
}