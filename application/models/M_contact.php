<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_contact extends CI_Model
{

    public $table = 'tb_contact';
    public $id = 'id_contact';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->row();
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

}