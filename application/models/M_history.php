<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_history extends CI_Model
{

    public $table = 'tb_history';
    public $id = 'id_history';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all_history_customers($id_user)
    {
        $this->db->select("tb_invoice.kode, tb_user.nama as nama_driver,tb_user.foto_user, tb_invoice.cost, tb_invoice.origin, tb_invoice.destination, tb_kendaraan.merk, tb_kendaraan.model, tb_history.create_on, tb_history.status");
        $this->db->order_by($this->id, $this->order);
        $this->db->join('tb_invoice', 'tb_invoice.kode = tb_history.invoice_id', 'left');
        $this->db->join('tb_user', 'tb_user.id_user = tb_invoice.driver_id', 'left');
        $this->db->join('tb_kendaraan', 'tb_user.id_user = tb_kendaraan.user_id', 'left');
        $this->db->where('tb_invoice.user_id',$id_user);
        return $this->db->get($this->table)->result();
    }

    function get_all_history_driver($id_user)
    {
        $this->db->select("tb_invoice.kode, tb_user.nama as nama_customer,tb_user.foto_user, tb_invoice.cost, tb_invoice.origin, tb_invoice.destination, tb_history.create_on, tb_history.status");
        $this->db->order_by($this->id, $this->order);
        $this->db->join('tb_invoice', 'tb_invoice.kode = tb_history.invoice_id', 'left');
        $this->db->join('tb_user', 'tb_user.id_user = tb_invoice.user_id', 'left');
        $this->db->where('tb_invoice.driver_id',$id_user);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_history_customers_by_id($id)
    {
        $this->db->select("tb_history.id_history, tb_invoice.kode, tb_invoice.wisata_id, tb_wisata.nama_wisata, tb_invoice.wisata, tb_user.nama as nama_driver,tb_user.foto_user, tb_invoice.cost, tb_invoice.origin, tb_invoice.destination, tb_kendaraan.merk, tb_kendaraan.model, tb_history.create_on, tb_history.status");
        $this->db->where($this->id, $id);
        $this->db->join('tb_invoice', 'tb_invoice.kode = tb_history.invoice_id', 'left');
        $this->db->join('tb_user', 'tb_user.id_user = tb_invoice.driver_id', 'left');
        $this->db->join('tb_kendaraan', 'tb_user.id_user = tb_kendaraan.user_id', 'left');
        $this->db->join('tb_wisata', 'tb_invoice.wisata_id = tb_wisata.id_wisata', 'left');
        return $this->db->get($this->table)->row();
    }

    function get_history_driver_by_id($id)
    {
        $this->db->select("tb_history.id_history, tb_invoice.kode, tb_invoice.wisata_id, tb_wisata.nama_wisata, tb_invoice.wisata, tb_user.nama as nama_customer,tb_user.foto_user, tb_invoice.cost, tb_invoice.origin, tb_invoice.destination, tb_history.create_on, tb_history.status");
        $this->db->where($this->id, $id);
        $this->db->join('tb_invoice', 'tb_invoice.kode = tb_history.invoice_id', 'left');
        $this->db->join('tb_user', 'tb_user.id_user = tb_invoice.user_id', 'left');
        $this->db->join('tb_wisata', 'tb_invoice.wisata_id = tb_wisata.id_wisata', 'left');
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}