<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pages extends CI_Model
{

    public $table = 'tb_pages';
    public $id = 'id_pages';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_kat_and_id($kat,$id)
    {
        $this->db->where('kategori', $kat);
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_kat_and_id_result($kat,$id)
    {
        $this->db->where('kategori', $kat);
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->result();
    }

    function get_by_kat($kat)
    {
        $this->db->where('kategori', $kat);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}