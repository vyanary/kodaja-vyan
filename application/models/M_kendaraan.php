<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kendaraan extends CI_Model
{

    public $table = 'tb_kendaraan';
    public $id = 'id_kendaraan';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_where($where)
    {
        $this->db->where($where);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_userid($userid)
    {
        $this->db->join('tb_user', 'tb_user.id_user = tb_kendaraan.user_id', 'right');
        $this->db->where('user_id', $userid);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_kendaraan', $q);
    	$this->db->or_like('merk', $q);
    	$this->db->or_like('model', $q);
        $this->db->or_like('warna', $q);
        $this->db->or_like('tahun', $q);
        $this->db->or_like('plat', $q);
        $this->db->or_like('wilayah', $q);
        $this->db->or_like('foto', $q);
        $this->db->or_like('user_id', $q);
    	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->like('id_kendaraan', $q);
        $this->db->or_like('merk', $q);
        $this->db->or_like('model', $q);
        $this->db->or_like('warna', $q);
        $this->db->or_like('tahun', $q);
        $this->db->or_like('plat', $q);
        $this->db->or_like('wilayah', $q);
        $this->db->or_like('foto', $q);
        $this->db->or_like('user_id', $q);
	    $this->db->limit($limit, $start);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function delete_by_userid($userid)
    {
        $this->db->where('user_id', $userid);
        $this->db->delete($this->table);
    }

}