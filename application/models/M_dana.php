<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dana extends CI_Model
{

    public $table = 'tb_dana';
    public $id = 'id_dana';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_by_id($kode)
    {
        $this->db->where($this->id, $kode);
        return $this->db->get($this->table)->row();
    }

    function get_by_invoice($kode)
    {
        $this->db->where('invoice_id', $kode);
        return $this->db->get($this->table)->row();
    }

    function cek_by_invoice($kode)
    {
        $this->db->where('invoice_id', $kode);
        return $this->db->get($this->table)->num_rows();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

}