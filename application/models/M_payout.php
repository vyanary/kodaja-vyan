<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_payout extends CI_Model
{

    public $table = 'tb_payout';
    public $id = 'id_payout';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->join('tb_user', 'tb_user.id_user = tb_payout.user_id', 'left');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_by_id($id_payout)
    {
        $this->db->where($this->id, $id_payout);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // get total rows
    function total_rows() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

}