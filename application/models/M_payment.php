<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_payment extends CI_Model
{

    public $table = 'tb_payment';
    public $id = 'id_payment';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get total rows
    function total_rows() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get all
    function get_all()
    {
        $this->db->join('tb_user', 'tb_user.id_user = tb_payment.id_user', 'left');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

}